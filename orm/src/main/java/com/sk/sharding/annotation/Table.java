package com.sk.sharding.annotation;

import com.sk.sharding.enums.ArithmeticEnum;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 分表注解
 */
@Target({java.lang.annotation.ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Table {

    /**
     * 表名前缀
     */
    public String prefix();

    /**
     * 分表字段
     */
    public String column();

    /**
     * 算法：可以直接%基数(ArithmeticEnum.MODE)，也可以取hashCode再%基数(ArithmeticEnum.HASH)
     */
    public ArithmeticEnum arithmetic();

    /**
     * 分表个数
     */
    public int count();


}
