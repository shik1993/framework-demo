package com.sk.sharding.po;

import java.util.Date;

/**
 * 实体基类
 */
public class Entity {

    /**
     * 主键
     */
    private transient Long id;

    /**
     * 创建时间
     */
    private transient Date createTime;

    /**
     * 修改时间
     */
    private transient Date updateTime;

    /**
     * 表名
     */
    private transient String tableName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}
