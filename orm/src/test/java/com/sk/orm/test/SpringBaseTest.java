package com.sk.orm.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * spring 代码测试的基类
 */
@ContextConfiguration(locations = { "classpath:/testContext.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class SpringBaseTest {
    private static final Logger logger = LoggerFactory.getLogger(SpringBaseTest.class);

    @Autowired
    private ThreadPoolExecutor taskThreadPoolExecutor;

    @Test
    public void test() throws InterruptedException {

        logger.debug("baseTest execute success ! {}", System.currentTimeMillis());
        logger.info("234");
    }

}
