package com.sk.designpattern.factory.demo.enums;

import com.google.common.collect.Maps;
import com.sk.designpattern.factory.demo.processor.DemoType1Service;
import com.sk.designpattern.factory.demo.processor.DemoType2Service;
import com.sk.designpattern.factory.demo.processor.DemoType3Service;

import java.util.Map;

/**
 * 服务方法枚举类
 */
public enum ServiceEnum {

    SERVICE_IMPL_1("demoType1Service", DemoType1Service.class),
    SERVICE_IMPL_2("demoType2Service", DemoType2Service.class),
    SERVICE_IMPL_3("demoType3Service", DemoType3Service.class),
    ;

    private String code;
    private Class<?> serviceClass;
    private static final Map<String, ServiceEnum> serviceMap = Maps.newHashMap();

    static {
        for (ServiceEnum serviceEnum : ServiceEnum.values()) {
            serviceMap.put(serviceEnum.getCode(), serviceEnum);
        }
    }

    public static ServiceEnum toEnum(String code) {
        return serviceMap.get(code);
    }

    ServiceEnum(String code, Class<?> serviceClass) {
        this.code = code;
        this.serviceClass = serviceClass;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Class<?> getServiceClass() {
        return serviceClass;
    }

    public void setServiceClass(Class<?> serviceClass) {
        this.serviceClass = serviceClass;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ServiceEnum{");
        sb.append("code='").append(code).append('\'');
        sb.append(", serviceClass=").append(serviceClass);
        sb.append('}');
        return sb.toString();
    }

    /**
     * 判断是否为canProcess服务
     */


    public static Boolean isService1(String serviceEnum){
        return ServiceEnum.SERVICE_IMPL_1.getCode().equalsIgnoreCase(serviceEnum);
    }

    public static Boolean isService2(String serviceEnum){
        return ServiceEnum.SERVICE_IMPL_2.getCode().equalsIgnoreCase(serviceEnum);
    }

    public static Boolean isService3(String serviceEnum){
        return ServiceEnum.SERVICE_IMPL_3.getCode().equalsIgnoreCase(serviceEnum);
    }

}
