package com.sk.designpattern.factory.demo.processor;

import com.sk.designpattern.factory.demo.DemoProcessService;
import com.sk.designpattern.factory.demo.enums.ServiceEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by shik on 2019/4/24.
 */
@Service
public class DemoType2Service implements DemoProcessService {
    private static final Logger logger = LoggerFactory.getLogger(DemoType2Service.class);


    public DemoType2Service() {
    }

    /**
     * 处理业务
     * @param content 请求信息
     * @param service 服务名称
     * @param version 接口版本号
     * @return 业务处理的结果
     */
    @Override
    public Boolean process(String content, String service, String version){
        logger.debug("DemoType2Service.deal {},service {},version {}",content,service,version);
        return true;
    }

    @Override
    public boolean canProcess(String serviceName) {
        return ServiceEnum.isService2(serviceName);
    }

}
