package com.sk.designpattern.factory.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 启动类
 * Created by shik on 2019/4/25.
 */
@ContextConfiguration(locations = { "classpath:/testContext.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class Bootstrap {
    private static final Logger logger = LoggerFactory.getLogger(Bootstrap.class);

    @Autowired
    private DemoFactoryService factoryService;

    @Test
    public void test() throws InterruptedException {
        logger.debug("baseTest execute success ! {}", System.currentTimeMillis());
        //DemoFactoryService factoryService = new DemoFactoryServiceImpl();
        factoryService.deal("11", "demoType1Service","1.0.0");

    }
}
