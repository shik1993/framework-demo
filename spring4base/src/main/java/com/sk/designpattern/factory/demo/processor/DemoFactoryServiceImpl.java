package com.sk.designpattern.factory.demo.processor;

import com.sk.designpattern.factory.demo.DemoFactoryService;
import com.sk.designpattern.factory.demo.DemoProcessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by shik on 2019/4/24.
 */
@Component
public  class DemoFactoryServiceImpl implements DemoFactoryService{
    private static final Logger logger = LoggerFactory.getLogger(DemoFactoryServiceImpl.class);

    public DemoFactoryServiceImpl() {
    }


    @Autowired
    private Map<String, DemoProcessService> allProcesssorMap;

    private ConcurrentMap<String, DemoProcessService> serviceConcMap = new ConcurrentHashMap<String, DemoProcessService>();

    //private static Map<String , DemoProcessService> allProcesssorMap;

    /**
     *     因为多线程环境下，使用Hashmap进行put操作会引起死循环，
     *     导致CPU利用率接近100%，所以在并发情况下不能使用HashMap。
     */
    //private ConcurrentHashMap<String , DemoProcessService> serviceConcMap = new ConcurrentHashMap<String, DemoProcessService>();

    /**
     * 去除static代码块模式
     * @param content 请求信息
     * @param service 服务名称
     * @param version 接口版本号
     * @return
     */
//    static {
//        allProcesssorMap = new HashMap<String, DemoProcessService>();
//        allProcesssorMap.put(ServiceEnum.SERVICE_IMPL_1.getCode(), new DemoType1Service());
//        allProcesssorMap.put(ServiceEnum.SERVICE_IMPL_2.getCode(), new DemoType2Service());
//        allProcesssorMap.put(ServiceEnum.SERVICE_IMPL_3.getCode(), new DemoType3Service());
//    }

    @Override
    public Boolean deal(String content, String service, String version) {
        logger.info("---------------start---------------");
        allProcesssorMap.get(service).process(content,service,version);

        logger.info("---------------end---------------");
        this.fillProcessor(service);
        return serviceConcMap.get(service).process(content,service, version);
    }


    public void fillProcessor(String clazzName) {
        if (!serviceConcMap.containsKey(clazzName)) {
            Collection<DemoProcessService> cols = allProcesssorMap.values();
            for (DemoProcessService processor : cols) {
                boolean canProcess = processor.canProcess(clazzName);
                if (canProcess) {
                    serviceConcMap.putIfAbsent(clazzName, processor);
                    break;
                }
            }
            if (!serviceConcMap.containsKey(clazzName)) {
                logger.error("DemoFactoryServiceImpl#fill processor failed, processorName={}", clazzName);
            }
        }
    }
}
