package com.sk.designpattern.factory.demo;

/**
 * Created by shik on 2019/4/24.
 */
public interface  DemoFactoryService {

    /**
     * 处理业务
     * @param content 请求信息
     * @param service 服务名称
     * @param version 接口版本号
     * @return 业务处理的结果
     */
    public Boolean deal(String content, String service, String version);

}
