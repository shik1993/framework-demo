package com.sk.designpattern.chain;

import org.apache.shardingsphere.elasticjob.lite.spring.boot.job.ScheduleJobBootstrapStartupRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 责任链启动类+参数传递上下文+切面(记录日志等)
 * Created by shik on 2019/4/25.
 */
@ContextConfiguration(locations = { "classpath:/testContext.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class Bootstrap {
    private static final Logger logger = LoggerFactory.getLogger(Bootstrap.class);

    @Autowired
    private ChainExecution chainExecution;

    @Test
    public void testChain(){
        //设置上下文参数
        ArgsContext context = new ArgsContext();
        chainExecution.executeStrategy(context);
        logger.info("testChain#end#context {}",context.toString());
        SchedulerException
    }

    @Test
    public void testChainByAnno(){
        //设置上下文参数
        ArgsContext context = new ArgsContext();
        chainExecution.executeStrategyByAnno(context);
        logger.info("testChain#end#context {}",context.toString());
    }
}
