package com.sk.designpattern.chain;

import java.util.List;

/**
 * Created by shik on 2019/6/5.
 */
public interface ChainStrageInterface {

    public List executeStrategy(ArgsContext arg);

}
