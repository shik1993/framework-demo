package com.sk.designpattern.chain;

/**
 * Created by shik on 2019/6/6.
 */
public class ArgsContext {

    private String chain1Result;
    private String chain2Result;
    private String chain3Result;
    private String chain4Result;

    public ArgsContext() {
    }

    public String getChain1Result() {
        return chain1Result;
    }

    public void setChain1Result(String chain1Result) {
        this.chain1Result = chain1Result;
    }

    public String getChain2Result() {
        return chain2Result;
    }

    public void setChain2Result(String chain2Result) {
        this.chain2Result = chain2Result;
    }

    public String getChain3Result() {
        return chain3Result;
    }

    public void setChain3Result(String chain3Result) {
        this.chain3Result = chain3Result;
    }

    public String getChain4Result() {
        return chain4Result;
    }

    public void setChain4Result(String chain4Result) {
        this.chain4Result = chain4Result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ArgsContext{");
        sb.append("chain1Result='").append(chain1Result).append('\'');
        sb.append(", chain2Result='").append(chain2Result).append('\'');
        sb.append(", chain3Result='").append(chain3Result).append('\'');
        sb.append(", chain4Result='").append(chain4Result).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
