package com.sk.designpattern.chain.aspectj;

import com.sk.designpattern.chain.ArgsContext;
import org.apache.commons.collections.CollectionUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 增强类(注解形式)
 * @Before 前置
 *   @AfterReturning  后置，可以获得返回值，必须在注解中确定返回值参数名称。
 *   @AfterThrowing 抛出异常，可以获得具体异常信息，必须在注解确定第二个参数名称
 *   @Around 环绕[]
 *   @After 最终
 */
@Aspect
@Component
public class ChainAnnoAspect {
    private static final Logger logger = LoggerFactory.getLogger(ChainAnnoAspect.class);

    @Pointcut(value="@annotation(com.sk.ChainMonitor) && args(context,..)")
    public void chainAnnoMonitor(ArgsContext context){
    }
//
//
    @Before("chainAnnoMonitor(context)")
    public void chainAnnoPointCutPre(ArgsContext context){
        System.out.println("chainAnnoPointCutPre#pre "+context);
    }


    @AfterReturning(pointcut = "chainAnnoMonitor(context)", returning = "list")
    public void chainResponseMonitor(ArgsContext context, List list) {
        if(CollectionUtils.isNotEmpty(list)) {
            list.forEach(item -> {
                logger.info("AfterReturning#chainAnnoMonitor: {}",item.toString());
            });
        }
    }

}
