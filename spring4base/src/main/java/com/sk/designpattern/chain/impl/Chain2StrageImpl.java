package com.sk.designpattern.chain.impl;

import com.google.common.collect.Lists;
import com.sk.designpattern.chain.ArgsContext;
import com.sk.designpattern.chain.ChainStrageInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by shik on 2019/6/6.
 */
public class Chain2StrageImpl implements ChainStrageInterface {
    private static final Logger logger = LoggerFactory.getLogger(Chain2StrageImpl.class);

    @Override
    public List executeStrategy(ArgsContext arg) {
        arg.setChain2Result("Chain2StrageImpl#OK");
        logger.info("Chain2StrageImpl#deal {}",arg.getChain2Result());
        return Lists.newArrayList("Chain2StrageImpl");
    }
}
