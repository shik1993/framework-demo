package com.sk.designpattern.chain.impl;

import com.google.common.collect.Lists;
import com.sk.designpattern.chain.ArgsContext;
import com.sk.designpattern.chain.ChainStrageInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by shik on 2019/6/6.
 */
public class Chain4StrageImpl implements ChainStrageInterface {
    private static final Logger logger = LoggerFactory.getLogger(Chain4StrageImpl.class);

    @Override
    public List executeStrategy(ArgsContext arg) {
        arg.setChain4Result("Chain4StrageImpl#OK");
        logger.info("Chain4StrageImpl#deal {}",arg.getChain4Result());
        return Lists.newArrayList("Chain4StrageImpl");
    }
}
