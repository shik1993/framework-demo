package com.sk.designpattern.chain.aspectj;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 增强类(pointCut表达式形式)
 * @Before 前置
@AfterReturning  后置，可以获得返回值，必须在注解中确定返回值参数名称。
@AfterThrowing 抛出异常，可以获得具体异常信息，必须在注解确定第二个参数名称
@Around 环绕[]
@After 最终
 */
@Aspect
@Component
public class ChainAspect {
    private static final Logger  logger = LoggerFactory.getLogger(ChainAspect.class);

//    @Pointcut("@annotation(* com.sk.ChainMonitor) && args(context,..)")
//    public void chainMonitor(ArgsContext context){
//        logger.info("chainMonitor {}",context);
//    }

    /**
     * Pointcut的定义包括两个部分：
     * Pointcut表示式(expression)和Pointcut签名(signature)
     */
    @Pointcut("execution(* com.sk.ChainExecution.*(..))")
    public void pointCut(){
    }

    @Before("pointCut()")
    public void pointCutPre(){
        logger.info("ponitCut#myAround#pre");
    }

    @Around("pointCut()")
    public Object myAround(ProceedingJoinPoint joinPoint) throws Throwable {
        logger.info("ponitCut#myAround#around#pre");
        Object obj = joinPoint.proceed();
        logger.info("ponitCut#myAround#around#after");
        return obj;
    }

    @After("pointCut()")
    public void pointCutAfter(){
        logger.info("ponitCut#myAround#after");
    }

    @AfterThrowing("pointCut()")
    public void pointCutAfterThrow(){
        System.out.println("ponitCut#myAround#afterThrow");
    }

//    @Before("pointCut()")
//    public void chainRequestMonitor() {
//        logger.info("chainRequestMonitor ");
//    }
//
//    @AfterReturning(pointcut = "chainMonitor(context)", returning = "list")
//    public void chainResponseMonitor(ArgsContext context, List list) {
//        if(CollectionUtils.isNotEmpty(list)) {
//            list.forEach(item -> {
//                logger.info("chainMonitor {}",item.toString());
//            });
//        }
//    }

}
