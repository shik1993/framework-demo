package com.sk.designpattern.proxy.cglib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 目标类 Target 目标类，需要被增强的类
 * Created by shik on 2019/6/10.
 * 如果将目标类设置为final,
 * public final class DemoServiceImpl{ 会爆出
 * java.lang.IllegalArgumentException:
 * Cannot subclass final class com.sk.DemoServiceImpl
 */
//public final class DemoServiceImpl{
public class DemoServiceImpl{
    private static final Logger logger = LoggerFactory.getLogger(DemoServiceImpl.class);

    /**
     * JointPoint：连接点，目标类上需要被增强的方法，
     * (这些方法可以被增强，也可以不增强，也就是说目标类中所有的方法都可以称为是连接点
     *
     * PointCut：切入点，
     * 被增强的方法(已经确定这个方法要被增强)，切入点就是一个连接点的子集
     */
    public void add() {
        logger.info("DemoServiceImpl#cglib proxy#add() ");
    }
}
