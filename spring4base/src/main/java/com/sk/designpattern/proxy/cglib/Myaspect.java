package com.sk.designpattern.proxy.cglib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 增强类 enhancer
 * Created by shik on 2019/6/10.
 */
public class Myaspect {
    private static final Logger logger = LoggerFactory.getLogger(Myaspect.class);

    /**
     * Advice：增强/通知，增强的代码，也就是上面将增强的代码凑成的一个类。
     * 类中的每个方法都代表一个增强的功能代码，这个类中的方法就被称为通知
     */
    public void beforeInvoke(){
        logger.info("Myaspect#cglib#beforeInvoke");
    }
    /**
     * Advice：增强/通知，增强的代码，也就是上面将增强的代码凑成的一个类。
     * 类中的每个方法都代表一个增强的功能代码，这个类中的方法就被称为通知
     */
    public void afterInvoke(){
        logger.info("Myaspect#cglib#afterInvoke");
    }
}
