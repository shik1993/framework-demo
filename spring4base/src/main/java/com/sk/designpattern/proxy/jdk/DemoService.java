package com.sk.designpattern.proxy.jdk;

/**
 * 接口
 * Created by shik on 2019/6/10.
 */
public interface DemoService {
    public void add();
}
