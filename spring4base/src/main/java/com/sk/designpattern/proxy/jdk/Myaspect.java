package com.sk.designpattern.proxy.jdk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 增强类 enhancer
 * Created by shik on 2019/6/10.
 */
public class Myaspect {
    private static final Logger logger = LoggerFactory.getLogger(Myaspect.class);

    public void beforeInvoke(){
        logger.info("Myaspect#jdk#beforeInvoke");
    }

    public void afterInvoke(){
        logger.info("Myaspect#jdk#afterInvoke");
    }
}
