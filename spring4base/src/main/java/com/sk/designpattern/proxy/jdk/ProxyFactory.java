package com.sk.designpattern.proxy.jdk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * jdk代理工厂类
 * Created by shik on 2019/6/10.
 */
public class ProxyFactory {
    private static final Logger logger = LoggerFactory.getLogger(ProxyFactory.class);
    //增强类
    final static Myaspect myAspect = new Myaspect();

    //将需要申请代理的类的对象传入,返回一个此对象的代理类对象
    public static Object createProxy(final Object o){
        /**
         * 参数一: 类加载器
         * 参数二:该对象所实现的接口
         * 参数三:增强方法的执行者,每次对方法的增强,实际上由他完成,这里用 匿名内部类.
         */
        Object obj = Proxy.newProxyInstance(o.getClass().getClassLoader(),
                o.getClass().getInterfaces(), new InvocationHandler() {
                    /**
                     * 每次代理对象调用方法时都会实际执行该方法
                     * @param proxy 代理对象
                     * @param method 对象执行的方法
                     * @param args 方法的参数
                     * @return
                     * @throws Throwable
                     */
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args)
                            throws Throwable {
                        //logger.info("InvocationHandler#invoke# proxy : {} ,method : {} ,args : {} .",proxy,method,args);
                        myAspect.beforeInvoke();//增强的代码
                        Object obj1 = method.invoke(o,args);//原来需要执行的方法
                        myAspect.afterInvoke();//增强的代码
                        return obj1;
                    }
                });
        return obj;
    }
}
