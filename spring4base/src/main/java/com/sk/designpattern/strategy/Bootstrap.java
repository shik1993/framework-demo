package com.sk.designpattern.strategy;

import com.sk.designpattern.strategy.enums.OrgChannelEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 策略模式启动类
 * Created by shik on 2019/4/25.
 */
@ContextConfiguration(locations = { "classpath:/testContext.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class Bootstrap {
    private static final Logger logger = LoggerFactory.getLogger(Bootstrap.class);

    @Autowired
    private DemoStrageManager demoStrageManager;

    @Test
    public void testStrageService(){
        String orgChannel = OrgChannelEnum.QUNAR.getCode();
        DemoStrageProvider provider = demoStrageManager.getProviderByStrage(orgChannel);
        if (provider == null){
            logger.error("provider is null {} ,",orgChannel);
            return;
        }
        provider.deal(orgChannel);
    }
}
