package com.sk.designpattern.strategy;

/**
 * Created by shik on 2019/6/5.
 */
public interface DemoStrageProvider {

    public void deal(String arg);

    public Boolean isSupported(String orgChannel);

}
