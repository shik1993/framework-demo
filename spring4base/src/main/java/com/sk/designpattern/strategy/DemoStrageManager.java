package com.sk.designpattern.strategy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by shik on 2019/6/5.
 */
@Service
public final class DemoStrageManager {

    @Autowired
    private List<DemoStrageProvider> demoStrageProviderList;


     final DemoStrageProvider getProviderByStrage(final String orgChannel){
         List<DemoStrageProvider> demoStrageProviders = demoStrageProviderList.stream()
                 .filter(e -> e.isSupported(orgChannel))
                 .collect(Collectors.toList());
         if(demoStrageProviders != null && demoStrageProviders.size() > 0)
             return demoStrageProviders.get(0);
         return null;
    }
}
