package com.sk.designpattern.strategy.impl;

import com.sk.designpattern.strategy.DemoStrageProvider;
import com.sk.designpattern.strategy.enums.OrgChannelEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by shik on 2019/6/5.
 */
@Service
public class QunarDemoStrageProvider implements DemoStrageProvider {
    private static final Logger logger = LoggerFactory.getLogger(QunarDemoStrageProvider.class);

    @Override
    public void deal(String arg) {
        logger.info("QunarDemoStrageProvider#deal# {}",arg);
    }

    @Override
    public Boolean isSupported(String orgChannel) {
        return OrgChannelEnum.isQunar(orgChannel);
    }
}
