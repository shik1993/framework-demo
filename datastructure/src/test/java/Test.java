import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Test {

public static void main(String[] args){
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    try {
        Date date = fmt.parse("2019-11-30 23:59:59");
        System.out.println(date.getTime());
        Date date2 = fmt.parse("2020-04-01 00:00:00");
        System.out.println(date2.getTime());
        Date date1 = fmt.parse("2020-03-31 23:59:59");
        System.out.println(date1.getTime());
        Date date3 = fmt.parse("2020-06-01 00:00:00");
        System.out.println(date3.getTime());
        Date date4 = fmt.parse("2020-04-15 00:00:00");
        System.out.println(date4.getTime());
        //1585929600000  4   1585756800000 2
        Date date5 = fmt.parse("2020-04-01 00:00:02");
        System.out.println(date5.getTime());
    } catch (ParseException e) {
        e.printStackTrace();
    }
}
}
