package com.sk.datastructure.linkedList;

/**
 * 一个链表的节点
 * 内容是 node
 * value类型：string
 *
 *
 * Created by shik on 2019/2/19.
 */
public class Node {

    private Boolean isFirst;
    /**
     * 节点的key
     */
    private String key ;
    /**
     * 节点的 值
     */
    private String value ;
    /**
     * 下一个指向
     */
    private Node next ;

    public Node(String key, String value, Node next) {
        this.key = key;
        this.value = value;
        this.next = next;
        isFirst = false;
    }

    public Node(String key, String value) {
        this.key = key;
        this.value = value;
        isFirst = false;
    }

    public Node(){
        super();
    }

    public Boolean getFirst() {
        return isFirst;
    }

    public void setFirst(Boolean first) {
        isFirst = first;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Node{");
        sb.append("isFirst=").append(isFirst);
        sb.append(", key='").append(key).append('\'');
        sb.append(", value='").append(value).append('\'');
        sb.append(", next=").append(next);
        sb.append('}');
        return sb.toString();
    }
}
