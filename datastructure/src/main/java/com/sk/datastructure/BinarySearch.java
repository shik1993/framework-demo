package com.sk.datastructure;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 二分查找
 * Created by shik on 2018/12/13.
 */
public class BinarySearch {
    private static final Logger logger = LoggerFactory.getLogger(BinarySearch.class);

    public static int[] array = new int[20];
    static{
        for(int i = 0;i<20;i++){
            array[i] = i+1;
        }
    }

    public void binarySearch(int[] array,int data){
        if(array == null || array.length == 0){
            return;
        }
        //定义下标
        int low = 0 ;
        int high = array.length-1;
        int mid = (low + high) / 2 ;
        while (low<=high){
            if(array[mid] == data) {
                logger.info("找到了数据 {} ，对应数据的下标为arr[ {} ]。",data,mid);
                return;
            }
            else if(array[mid] > data){
                logger.info("大了");
                high = mid - 1;
                mid = (high + low) / 2;
            }
            else if (array[mid] <data){
                logger.info("小了");
                low = mid + 1;
                mid = (high + low) / 2;
            }
            else{
                logger.info("出错了");
            }
        }

    }

    @Test
    public void testBinarySearch(){

        for(int i = 0 ; i <array.length;i++)
        logger.info("第{}个元素 内容为： {} 。",i,array[i]);
        logger.info("开始二分查找\r\n");
        binarySearch(array,9);
    }
}
