package com.sk.datastructure.tree;

import com.google.common.collect.Lists;
import com.sk.datastructure.App;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Objects;

/**
 * Created by shik on 2019/2/16.
 */
public class BinaryTreeApp implements App {
    private static final Logger logger = LoggerFactory.getLogger(BinaryTreeApp.class);


    public BinaryTreeApp() {
        super();
        buildDataStracture();
    }

    @Override
    public Object buildDataStracture() {
        return  null;
    }

    @Test
    @Override
    public void start() {
        logger.debug("binaryTreeApp start");
        BinaryTreeApp binaryTreeApp = new BinaryTreeApp();

        BinNode node6 = new BinNode("6","4","6",null,null);
        BinNode node4 = new BinNode("4","2","4",node6,null);
        BinNode node5 = new BinNode("5","2","5",null,null);
        BinNode node2 = new BinNode("2","1","2",node4,node5);
        BinNode node7 = new BinNode("7","3","7",null,null);
        BinNode node3 = new BinNode("3","1","3",null,node7);
        BinNode rootNode = new BinNode("1","1","1-root",node2,node3);

        binaryTreeApp.displayBinNode(rootNode);


    }

    /**
     * 前序遍历
     * @param rootNode
     */
    private void displayBinNode(BinNode rootNode) {
        if(Objects.isNull(rootNode))
            return;
        System.out.println(rootNode.getText());
        if(!Objects.isNull(rootNode.getLeftNode())){
            displayBinNode(rootNode.getLeftNode());
        }
        if(!Objects.isNull(rootNode.getRightNode())){
            displayBinNode(rootNode.getRightNode());
        }
    }

    private BinNode createBinTree(BinNode rootNode , List<BinNode> binaryNodeList) {
        return null;
    }
}
