package com.sk.datastructure.recursion;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 递归的二分查找和非递归的二分查找效率都为O(logN)，
 * 递归的二分查找更加简洁，便于理解，但是速度会比非递归的慢。
 * 二分查找也称折半查找（Binary Search），它是一种效率较高的查找方法。但是，折半查找要求线性表必须采用顺序存储结构，而且表中元素按关键字有序排列
 * 递归
 * 二分查找
 * Created by shik on 2019/2/27.
 */
public class TwoFindSearch {
    private static final Logger logger = LoggerFactory.getLogger(TwoFindSearch.class);

    /**
     * 找到目标值返回数组下标，找不到返回-1
     * @param array
     * @param key
     * @return
     */
    public static int findTwoPoint(int[] array,int key){
        int start = 0;
        int last = array.length-1;
        while(start <= last){
            int mid = (last-start)/2+start;//防止直接相加造成int范围溢出 或者  (last + start) / 2,但是相减好一点，防溢出
            if(key == array[mid]){//查找值等于当前值，返回数组下标
                return mid;
            }
            if(key > array[mid]){//查找值比当前值大
                start = mid+1;
            }
            if(key < array[mid]){//查找值比当前值小
                last = mid-1;
            }
        }
        return -1;
    }

    /**
     * 使用递归进行二分查找
     * @param array
     * @param key
     * @param low
     * @param high
     * @return
     */
    public static int search(int[] array,int key,int low,int high){
        int mid = (high-low)/2+low;
        if(key == array[mid]){//查找值等于当前值，返回数组下标
            return mid;
        }else if(low > high){//找不到查找值，返回-1
            return -1;
        }else{
            if(key < array[mid]){//查找值比当前值小
                return search(array,key,low,mid-1);
            }
            if(key > array[mid]){//查找值比当前值大
                return search(array,key,mid+1,high);
            }
        }
        return -1;
    }

    /**
     * 二分by me
     * @param arr
     * @param value
     * @return
     */
    public int twoPartSearch(int[] arr,int value){
        int low = 0;
        int high = arr.length-1 ;
        int mid = 0;
        while(low <= high){
             mid = (low + high) / 2 ;
            logger.info("经过了一轮查找 mid {} , low {} , high {}",mid, low , high);
            if(arr[mid] == value){
                return value;
            }else if(arr[mid] < value){
                low = mid+1;
            }else if(arr[mid] > value){
                high = mid-1;
            }
        }

        if(arr[mid] == value){
            return value;
        }
        return -1;
    }

    /**
     * 递归查询 by me
     * @param arr
     * @param value
     * @return
     */
    public int twoPartSearchDG(int[] arr,int value,int low,int high){
        int mid =( low + high )/2 ;
        if(low <= high){
            logger.info("经过了一轮查找 mid {} , low {} , high {}",mid, low , high);
            if(arr[mid] == value){
                return arr[mid];
            }else if(arr[mid] < value){
                low = mid+1;
                return twoPartSearchDG(arr,value,low,high);
            }else if(arr[mid] > value){
                high = mid - 1;
                return twoPartSearchDG(arr,value,low,high);
            }
        }
        return -1;
    }

    @Test
    public void testTwoPartSearch(){
        TwoFindSearch twoFindSearch = new TwoFindSearch();

        int a[] = {1,4,5,6,7,8,10};
        logger.info("result : {}",twoPartSearch(a,9));
        logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        logger.info("resultDG : {}",twoPartSearchDG(a,9,0,6));

    }




}
