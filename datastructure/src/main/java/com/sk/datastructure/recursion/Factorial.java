package com.sk.datastructure.recursion;

import com.sk.datastructure.AppAbs;
import org.junit.Test;

/**
 * 使用递归求阶乘
 * n! = n*(n-1)*(n-2)*......1
 * n! = n*(n-1)！
 * Created by shik on 2019/2/27.
 */
public class Factorial extends AppAbs{

    @Override
    @Test
    public void start() {
        Factorial factorial = new Factorial();
        int n = 10;
        factorial.display(n+"!="+ getFactorial(10));

    }

    /**
     * 使用递归
     * 0！=1  1！=1
     * 负数没有阶乘,如果输入负数返回-1
     * @param n
     * @return
     */
    public static int getFactorial(int n){
        if(n >= 0){
            if(n==0){
                //System.out.println(n+"!=1");
                return 1;
            }else{
                //System.out.println(n);
                int temp = n*getFactorial(n-1);
                //System.out.println(n+"!="+temp);
                return temp;
            }
        }
        return -1;
    }

    /**
     * 使用for循环，不用递归
     * 0！=1  1！=1
     * 负数没有阶乘,如果输入负数返回-1
     * @param n
     * @return
     */
    public static int getFactorialFor(int n){
        int temp = 1;
        if(n >=0){
            for(int i = 1 ; i <= n ; i++){
                temp = temp*i;
            }
        }else{
            return -1;
        }
        return temp;
    }


}
