package com.sk.datastructure.recursion;

import static com.sk.datastructure.sort.SortUtils.display;

/**
 * 使用递归的方式实现
 * 数组的乘积
 * 体现的是一种 分治处理的思想
 */
public class CalArrayWithRecursion {


    public static void main(String[] args){
        int[] arr = new int[]{2,4,6,8,10};
        //基准条件
        if(arr.length >1) {
            int result = cal(arr);
            System.out.print("result: "+result);
        }
    }

    /**
     * 循环条件
     * @param arr
     * @return
     */
    private static int cal(int[] arr) {
        //基准条件
        if(arr.length <=1){
            return arr[0];
        }
        int[] arrRight = new int[arr.length-1];
        for(int i=1;i<arr.length;i++){
            arrRight[i-1] = arr[i];
        }
        System.out.print("cal :" + arr[0] +"* 分治数组 ");
        display(arrRight);
        return arr[0] * cal(arrRight);
    }


}
