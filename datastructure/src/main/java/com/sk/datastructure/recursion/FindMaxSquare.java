package com.sk.datastructure.recursion;

/**
 * 题目如下:
 * 现在给出一个长方形,比如说160 * 30
 * 那么找到这个长方形能均匀的切割成正方形,的所有可能中实现方块都是最大的,且没有浪费
 * 长  1680,宽640
 *
 */
public class FindMaxSquare {
    public static volatile int i = 1;

    public static void main(String[] args){
        int length = 1680;
        int width = 640;
        if(width == 0){
            return;
        }
        if(length % width < width){
            cal(length,width);
        }
    }

    private static void cal(int length, int width) {
        if(width == 0){
            return;
        }
        if(length % width == 0){
            System.out.println("最后一块正方形:"+width +"*"+width+",数量为:"+length/width+".");
        }
        System.out.println("第"+ i++ +"块正方形:"+width +"*"+width+",数量为:"+length/width+".");
        if(length % width < width){
            cal(width,length % width);
        }
    }

    /**
     * 解题思路 是 分而治之
     * 使用递归的方式求
     * 基准条件:
     * 长度是 宽度的 N次方
     */
}
