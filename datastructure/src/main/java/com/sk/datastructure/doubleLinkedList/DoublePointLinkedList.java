package com.sk.datastructure.doubleLinkedList;

import com.sk.datastructure.App;
import org.junit.Test;

/**
 * 双端链表
 * Created by shik on 2019/2/20.
 */
public class DoublePointLinkedList implements App{

    private int size;
    private Node head;
    private Node tair;


    public DoublePointLinkedList() {
        super();
        size = 0 ;
        head = null;
        tair = null;
    }

    public Node query(Object value){
        int tempSize = size;
        Node tempNode = head;
        while(tempSize > 0){
            if(tempNode.getData().equals(value)){
                return tempNode;
            }
            tempNode = tempNode.getNext();
            tempSize--;
        }
        return null;
    }

    //删除头部节点，成功返回true，失败返回false
    public boolean deleteHead(){
        if(size == 0){//当前链表节点数为0
             return false;
        }
        if(head.next == null){//当前链表节点数为1
          head = null;
          tair = null;
          }else{
              head = head.next;
          }
            size--;
            return true;
        }

    //判断是否为空
    public boolean isEmpty(){
        return (size ==0);
    }

    //获得链表的节点个数
    public int getSize(){
        return size;
    }

    public Node addHead(Object value){
        Node newHead = new Node(value);
        if(head == null){
            head = newHead;
            tair = newHead;
            size++;
            return head;
        }
        newHead.setNext(head);
        head = newHead;
        size++;
        return newHead;
    }

    public Node addTair(Object value){
        Node newTair = new Node(value);
        if(head == null){
            head = newTair;
            tair = newTair;
            size++;
            return head;
        }
        tair.setNext(newTair);
        tair = newTair;
        size++;
        return head;

    }

    public void display(){
        if(size >0){
            Node node = head;
            int tempSize = size;
            if(tempSize == 1){//当前链表只有一个节点
                System.out.println("["+node.data+"]");
                return;
            }
            while(tempSize > 0){
                if(node.equals(head)){
                    System.out.print("["+node.data+"->");
                }else if(node.next == null){
                    System.out.print(node.data+"]");
                }else{
                    System.out.print(node.data+"->");
                }
                node = node.next;
                tempSize--;
            }
            System.out.println();
        }else{//如果链表一个节点都没有，直接打印[]
            System.out.println("[]");
        }

    }


    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getTair() {
        return tair;
    }

    public void setTair(Node tair) {
        this.tair = tair;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DoublePointLinkedList{");
        sb.append("size=").append(size);
        sb.append(", head=").append(head);
        sb.append(", tair=").append(tair);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public Object buildDataStracture() {
        DoublePointLinkedList doublePointLinkedList = new DoublePointLinkedList();
        doublePointLinkedList.addHead("A");
        doublePointLinkedList.addHead("B");
        doublePointLinkedList.addHead("C");
        doublePointLinkedList.addTair("D");
        doublePointLinkedList.addTair("E");
        doublePointLinkedList.addTair("F");
        doublePointLinkedList.display();
        return doublePointLinkedList;
    }

    @Test
    @Override
    public void start() {
        DoublePointLinkedList node =(DoublePointLinkedList) buildDataStracture();
        if(node.deleteHead())
            node.display();
        Node value = node.query("B");
        System.out.println("query Node B :" + value );
    }

    private class Node{
        private Object data;
        private Node next;

        public Node(Object data) {
            this.data = data;
        }

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Node{");
            sb.append("data='").append(data).append('\'');
            sb.append(", next=").append(next);
            sb.append('}');
            return sb.toString();
        }
    }




}
