package com.sk.datastructure.stacks;

import com.sk.datastructure.App;
import org.junit.Test;

/**
 * 使用数组来实现一个 栈
 * 约定一个前提： 这个栈只能存正数，存-1代表空
 */
public class StackArray implements App {
    private int[] arr;
    private int size;

    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public Object buildDataStracture() {
        arr = new int[]{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
        size = 0;
        return arr;
    }

    /**
     * 压栈
     * @param value
     */
    public void pop(int value){
        int length = arr.length;
        if(size >= length){
            System.out.println("栈满！需要出栈");
        }
        for(int i = 0;i < arr.length;i++){
            if(arr[i] == -1){
                arr[i] = value;
                break;
            }
        }
        size++;
    }

    /**
     * 出栈
     * @return
     */
    public int push(){
        for(int i = arr.length-1;i >=0;i--){
            if(arr[i] != -1){
                int temp = arr[i] ;
                arr[i] = -1;
                size--;
                return temp;
            }
        }
        return -2;
    }

    public void display(int[] arr){
        System.out.println("*********display **********");
        for(int i =0;i<arr.length;i++){
            System.out.print(arr[i]+" \t");
        }
        System.out.println();
    }

    @Test
    @Override
    public void start() {
        StackArray stackArray = new StackArray();
        arr = (int[])stackArray.buildDataStracture();
        stackArray.display(arr);
        stackArray.pop(1);
        stackArray.display(arr);
        stackArray.pop(3);
        stackArray.display(arr);
        stackArray.pop(2);
        stackArray.pop(2);
        stackArray.display(arr);
        //push
        stackArray.push();
        stackArray.display(arr);
    }
}
