package com.sk.datastructure.arrays;

import com.sk.datastructure.linkedList.Node;

public class TestApp {
    /**
     * 测试动态数组
     * @param args
     */
    public static void main(String[] args){
        ArrayList dArrayList = new ArrayList<Node>();
        dArrayList.add("123");
        dArrayList.add("456");
        int indexOf = dArrayList.indexOf("456");
        System.out.println(indexOf);
        dArrayList.add(new Node("1","1",null));
        System.out.println(dArrayList.get(2));
    }
}
