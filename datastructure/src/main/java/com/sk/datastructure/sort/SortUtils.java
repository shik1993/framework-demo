package com.sk.datastructure.sort;

import org.junit.Test;

/**
 * Created by shik on 2019/2/22.
 */
public class SortUtils {

    //遍历显示数组
    public static void display(int[] array){
        for(int i = 0 ; i < array.length ; i++){
            System.out.print(array[i]+" ");
        }
        System.out.println();
    }

    /**
     * 气泡往水面冒，越来越大，每次都选出一个最大的气泡
     * 数组的长度是 size
     * 双层for循环
     * 冒泡排序一共要进行 size-1次排序
     * 每轮比较后的元素个数都会少一个，这也是为什么 j 的范围是逐渐减小的
     * 假设参与比较的数组元素个数为 N，则第一轮排序有 N-1 次比较，第二轮有 N-2 次，如此类推，这种序列的求和公式为：
     *   (N-1）+（N-2）+...+1 = N*（N-1）/2  约等于 N*N
     *   用大O表示法： O(N*N)
     *   N*（N-1）/2 次比较， N*（N-1）/2 次交换
     * @param initArray
     * @return
     */
    public static void bubbleSortDESC(int[] initArray){
        if(initArray ==null || initArray.length==0)
            return ;
        int size = initArray.length;
        for(int i = 0;i<size-1;i++){
            for(int j=i+1;j<size;j++){
                if(initArray[i] < initArray[j]) {
                    swap( initArray,  i,  j);
                }
            }
            System.out.print("第"+ ( i+1 )+"轮冒泡排序后的结果为:");
            display(initArray);
        }
        return ;
    }

    /**
     * 选择排序
     * 双层for循环，每次选择最小的元素放在最左边
     *  N*（N-1）/2 次比较， N 次交换，交换的次数少于冒泡排序
     *  所以效率高于冒泡排序
     */
    public static void choiceSort(int[] initArray){
        if(initArray == null || initArray.length == 0)
            return;
        for(int i=0;i<initArray.length-1;i++){
            int min=i; //每一次遍历时都把min置为最左边元素的下标。
            for(int j=i+1;j<initArray.length;j++){
                if(initArray[j] < initArray[min])
                    min = j;
            }
            swap(initArray, i, min);
            System.out.print("第"+ ( i+1 )+"轮选择排序后的结果为:");
            display(initArray);
        }

    }

    /**
     * 插入排序
     * 移动次数是N*N
     */
    public static int[] insertSort(int[] array){
            int j;
            //从下标为1的元素开始选择合适的位置插入，因为下标为0的只有一个元素，默认是有序的
            for(int i = 1 ; i < array.length ; i++){
                int tmp =  array[i];//记录要插入的数据
                j = i;
                while(j > 0 && tmp < array[j-1]){//从已经排序的序列最右边的开始比较，找到比其小的数
                    array[j] = array[j-1];//向后挪动
                    j--;
                }
                array[j] = tmp;//存在比其小的数，插入
            }
            return array;
    }

    /**
     * 冒泡正序(推荐定稿)
     * 第一个for（）记录的是总共需要比较多少轮的次数
     * 第二个for（）负责交换元素，每次选举出来一个最大的，放在最右边
     * flag的意思是当没有待排序的元素时，直接break；减少交换的次数
     * @param initArray
     */
    public static void bubbleSortASC(int[] initArray) {
        if(initArray ==null || initArray.length==0)
            return ;
        for(int i = initArray.length-1;i>0;i--){
            for(int j=0;j<i;j++){
                if(initArray[i] < initArray[j]) {
                    swap(initArray, i, j);
                }
            }
            System.out.print("第"+ ( i+1 )+"轮冒泡排序后的结果为:");
            display(initArray);
        }
        return ;
    }



    static void swap(int[] arr , int left,int right){
        int temp = arr[left] ;
        arr[left] = arr[right];
        arr[right] = temp;
    }

    @Test
    public void test() {
        int[] arr = {4, 3, 5, 7, 3, 2, 1};
        System.out.println("-------------冒泡正序--------------------------");
        SortUtils.bubbleSortASC(arr);
        System.out.println("-------------冒泡倒序--------------------------");
        SortUtils.bubbleSortDESC(arr);
        System.out.println("-------------选择排序--------------------------");
        int[] arr1 = {4, 3, 5, 7, 3, 2, 1};
        SortUtils.choiceSort(arr1);
        System.out.println("---------------------------------------");

        int[] arr3 = {4, 3, 5, 7, 3, 2, 1};
        //SortUtils.insertSort2(arr3);
        display(arr3);

    }

}
