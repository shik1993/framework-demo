package com.sk.datastructure.sort;

/**
 * (定稿)
 * 三种快速排序,我认为第三种最好理解
 */
public class FastSort3 extends SortUtils{

    public static void main(String[] args){
        int[] arr = new int[]{0,7,2,3,4,5,6,1};
        display(arr);
        quickSort(arr,0,7);
        display(arr);
    }

    public static void quickSort(int[] arr,int left,int right){
        if(left >= right)
            return;
        int index = arr[left];
        int i = left -  1 ;
        int j = right + 1  ;
        while ( i < j) {
            do {
                i++;
            } while (arr[i] < index);
            do {
                j--;
            } while (arr[j] > index);
            if(i< j){
                swap( arr ,  i, j);
            }
        }
        quickSort(arr,left,j);
        quickSort(arr,j+1,right);

    }


}
