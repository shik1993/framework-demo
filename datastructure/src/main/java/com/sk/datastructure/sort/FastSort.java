package com.sk.datastructure.sort;

/**
 * 快速排序
 */
public class FastSort {

    public static void main(String[] args){

        int[] arr = new int[]{6,2,3,4,5,1,7,10,8,9};
        FastSort.quicksort(arr,0,9);
        for(int a : arr){
            System.out.print(a+"\t");
        }
    }

    /**
     * @param pData
     * @param left
     * @param right
     * @return
     */
    static void quicksort(int[] n, int left, int right) {
        int dp;
        if (left < right) {
            dp = partition(n, left, right);
            quicksort(n, left, dp - 1);
            quicksort(n, dp + 1, right);
        }
    }

    static int partition(int n[], int left, int right) {
        int pivot = n[left];
        while (left < right) {
            while (left < right && n[right] >= pivot)
                right--;
            if (left < right)
                n[left++] = n[right];
            while (left < right && n[left] <= pivot)
                left++;
            if (left < right)
                n[right--] = n[left];
        }
        n[left] = pivot;
        return left;
    }
}
