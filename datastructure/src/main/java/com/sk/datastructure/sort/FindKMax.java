package com.sk.datastructure.sort;

public class FindKMax {

    public static void main(String[] args){

        int[] arr = new int[]{1,1,2,3,4,5,6,7,6,5,4,3,1};
        FindKMax test123 = new FindKMax();
        int result = test123.getResult(arr,4);
        System.out.println("result: "+ result );

        int result2 = test123.getResult(arr,20);
    }

    /**
     * 使用冒泡方式
     * @param arr
     * @param k
     * @return
     */
    private  int getResult(int[] arr,int k) {
        if(arr == null || arr.length == 0){
            return  -1;
        }
        int count =0;
        for(int i = arr.length-1; i >=0;i--){
            for(int j = 0;j < i; j++){
                if(arr[i] < arr[j]){
                    swap(arr,i,j);
                }
            }
            if((i < arr.length - 1 && arr[i] < arr[i+1])
                    || i == arr.length -1) {
                count++;
            }
            if(count >= k){
                return arr[i];
            }
        }

        return -1;
    }

    private void swap(int[] arr,int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }


}
