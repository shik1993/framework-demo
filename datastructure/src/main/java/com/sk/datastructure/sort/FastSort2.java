package com.sk.datastructure.sort;

/**
 * 高效的分治排序
 * 快速排序是冒泡排序的改进版，是目前已知的最快的排序方法。
 * 该排序算法的基本思想是：
 * 1．先从数列中取出一个数作为基准数。
 * 2．分区过程，将比这个数大的数全放到它的右边，小于或等于它的数全放到它的左边。
 * 3．再对左右区间重复第二步，直到各区间只有一个数。
 * 优点：极快，数据移动少；
 * 缺点：不稳定。
 *
 *
 *
 * 不稳定。不稳定。不稳定。不稳定。
 */
public class FastSort2 {

    public static void quickSort(int[] arr,int low,int high){
        int i,j,temp,t;
        if(low>high){
            return;
        }
        i=low;
        j=high;
        //temp就是基准位
        temp = arr[low];
        while (i<j){
            //先看右边，依次往左递减,直到temp大于基本数
            while (temp<=arr[j]&&i<j){
                j--;
            }
            //再看左边，依次往右递增，直到temp小于基本数
            while (temp>=arr[i]&&i<j){
                i++;
            }
            //如果满足条件则交换
            if (i<j){
                t=arr[j];
                arr[j]=arr[i];
                arr[i]=t;
            }
        }
        //最后将基准为与i和j相等位置的数字交换
        arr[low] = arr[i];
        arr[i] = temp;
        //递归调用左半数组
        quickSort(arr, low, j-1);
        //递归调用右半数组
        quickSort(arr, j+1, high);
    }

    public static void main(String[] args){
        int[] arr = {6,1,2,7,9,11,4,5,10,8};
        quickSort(arr, 0, arr.length-1);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}
