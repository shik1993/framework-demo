package com.sk.datastructure.mult_tree;

import java.util.List;

public class Node {
    private String id;          //结点id
    private String name;        //结点名称
    private List<Node> sonList; //该结点的 子结点集合

    public Node() {
        super();
    }

    public Node(String id, String name, List<Node> sonList) {
        this.id = id;
        this.name = name;
        this.sonList = sonList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Node> getSonList() {
        return sonList;
    }

    public void setSonList(List<Node> sonList) {
        this.sonList = sonList;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Node{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", sonList=").append(sonList);
        sb.append('}');
        return sb.toString();
    }
}

