package com.sk.datastructure.mult_tree;

import com.sk.datastructure.App;
import org.junit.Test;

/**
 * 多叉树测试
 * Created by shik on 2019/3/15.
 */
public class MultTreeTest implements App{

    @Override
    public Object buildDataStracture() {

        return null;
    }

    @Test
    @Override
    public void start() {
        TreeUtils treeUtils = new TreeUtils();
        Node node2 = new Node("2","node2",null);
        Node node3 = new Node("3","node3",null);
        Node node4 = new Node("4","node4",null);
        Node node5 = new Node("5","node5",null);
        Node root = new Node("1","root",null);

        treeUtils.insert(root,"1",node2);
        treeUtils.insert(root,"2",node3);
        treeUtils.insert(root,"2",node4);
        treeUtils.insert(root,"1",node4);
        treeUtils.insert(root,"1",node5);

        treeUtils.queryAll(root,1);
    }

}
