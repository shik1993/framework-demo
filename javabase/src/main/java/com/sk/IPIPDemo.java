package com.sk;

import net.ipip.ipdb.City;
import net.ipip.ipdb.CityInfo;
import net.ipip.ipdb.District;
import net.ipip.ipdb.DistrictInfo;

import java.util.Arrays;
import java.util.Map;

public class IPIPDemo {

    public static void main(String[] args){
        System.out.println("start ...");
        String ip = "2408:821a:1000::9" ;
        /***
         * 1.36.29.255
         *  23.19.235.255
         * 27.190.254.25
         * 49.208.0.0   陕西西安
         *
         *
         * 2408:821a:1700::1
         * 2408:821a:1000::9
         */
        try {
            // City类
            City db = new City("E:\\softLibrary\\ipip\\ipip_cn.ipdb");

            // db.find(address, language) 返回索引数组
            System.out.println("result : " + Arrays.toString(db.find(ip, "CN")));
            System.out.println("################################");
            // db.findInfo(address, language) 返回 CityInfo 对象
            CityInfo info = db.findInfo(ip, "CN");
            System.out.println("result : " + info);
            System.out.println("################################");
            District district = new District("E:\\softLibrary\\ipip\\ipip_cn.ipdb");

            System.out.println(Arrays.toString(district.find(ip, "CN")));
            System.out.println("################################");
            DistrictInfo districtInfo = district.findInfo(ip, "CN");
            if (info != null) {
                System.out.println(info);
                System.out.println("################################");
                System.out.println(info.getCountryName());
                System.out.println("################################");
            }

            Map m = db.findMap(ip, "CN");

            System.out.println(m);
            System.out.println("################################");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
