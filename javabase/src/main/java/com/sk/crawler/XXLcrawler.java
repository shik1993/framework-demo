package com.sk.crawler;

import com.xuxueli.crawler.XxlCrawler;
import com.xuxueli.crawler.annotation.PageFieldSelect;
import com.xuxueli.crawler.annotation.PageSelect;
import com.xuxueli.crawler.parser.PageParser;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *
 A distributed web crawler framework.（分布式爬虫框架XXL-CRAWLER）
 http://www.xuxueli.com/xxl-crawler
 * Created by shik on 2019/4/25.
 */
public class XXLcrawler {

    private static final Logger logger = LoggerFactory.getLogger(XXLcrawler.class);

    @Test
    public void test() throws InterruptedException {
        logger.debug("baseTest execute success ! {}", System.currentTimeMillis());
        logger.info("234");
        XxlCrawler crawler = new XxlCrawler.Builder()
                .setUrls("https://my.oschina.net/xuxueli/blog")
                .setWhiteUrlRegexs("https://my\\.oschina\\.net/xuxueli/blog/\\d+")
                .setThreadCount(3)
                .setPageParser(new PageParser<PageVo>() {
                    @Override
                    public void parse(Document html, Element pageVoElement, PageVo pageVo) {
                        // 解析封装 PageVo 对象
                        String pageUrl = html.baseUri();
                        System.out.println("content : "+pageUrl + "：" + pageVo.toString());
                    }
                })
                .build();
        crawler.start(true);
    }

    // PageSelect 注解：从页面中抽取出一个或多个VO对象；
    @PageSelect(cssQuery = "body")
    public static class PageVo {

        @PageFieldSelect(cssQuery = ".blog-heading .title")
        private String title;

        @PageFieldSelect(cssQuery = "#read")
        private int read;

        @PageFieldSelect(cssQuery = ".comment-content")
        private List<String> comment;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getRead() {
            return read;
        }

        public void setRead(int read) {
            this.read = read;
        }

        public List<String> getComment() {
            return comment;
        }

        public void setComment(List<String> comment) {
            this.comment = comment;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("PageVo{");
            sb.append("title='").append(title).append('\'');
            sb.append(", read=").append(read);
            sb.append(", comment=").append(comment);
            sb.append('}');
            return sb.toString();
        }
    }

}
