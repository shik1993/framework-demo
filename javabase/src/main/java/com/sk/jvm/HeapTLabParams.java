package com.sk.jvm;

/**
 * 设置栈上内存：tlab，jvm默认是开启这个状态的
 * 经过试验可以看到：开启tlab后，代码的执行时间大大的缩短（加速了对象的分配速度）
 *参数
 * -XX:+UseTLAB :开启使用tlab缓存，  -XX:-UseTLAB 不使用tlab缓存
 * -XX:TLABSize=102400 设置tlab的大小
 * -XX:TLABRefillWasteFraction=100 设置进入tlab的单个对象的大小，这是一个比例，比如说设置为100，意思就是 如果单个对象大小> 102400/100,就存入堆空间
 * -XX:-DoEscapeAnalysis 禁用逃逸检测
 * -XX:+PrintTLAB ：打印tlab
 * -XX:+PrintGC
 */
public class HeapTLabParams {
    public static void alloc() {
        byte[] b = new byte[2];
    }

    /**
     * 开启使用tlab： 耗时700ms
     * -XX:+UseTLAB -XX:TLABSize=102400 -XX:TLABRefillWasteFraction=100 -XX:-DoEscapeAnalysis -XX:+PrintTLAB -XX:+PrintGC
     * 关闭使用tlab： 耗时2500ms
     * -XX:-UseTLAB -XX:TLABSize=102400 -XX:TLABRefillWasteFraction=100 -XX:-DoEscapeAnalysis -XX:+PrintTLAB -XX:+PrintGC

     *
     * @param args
     */
    public static void main(String[] args) {
        //迭代一亿次
        long start = System.currentTimeMillis();
        for(int i = 0; i < 100000000; ++i) {
            alloc();
        }
        long end = System.currentTimeMillis();
        System.out.println("codes run time"+(end - start));

    }
}
