package com.sk.jvm;

/**
 * 设置生产dump文件并分析
 * 设置最大堆内存为2m，但是实际程序使用了5m >2m
 *
 */
public class HeapDump {
    /**
     * 设置vm params
     *  -Xms2m -Xmx2m -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=d:/HeapDump.dump
     * @param args
     */
    public static void main(String[] args){
        byte[] b=null;
        for(int i=0;i<10;i++){
            b = new byte[1*1024*1024];
        }
    }
}
