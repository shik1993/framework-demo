package com.sk.jvm;

/**
 * 使用java运行的方式来设置 gc堆内存
 * 右键 run confiuration设置 program params 代表的 主函数的入参设置
 * 右键 run confiuration设置 vm params 代表的 jvm堆信息的设置
 * -Xms5m 堆内存的初始值
 * -Xmx20m 最大堆内存值
 */
public class HeapGCParam01 {

    /**
     * 先随便执行一下main
     *然后设置 vm params
     * -XX:+PrintGC -Xms5m -Xmx20m -XX:+UseSerialGC -XX:+PrintGCDetails -XX:+PrintCommandLineFlags
     * 再执行一下main
     *
     * @param args
     */
    public static void main(String[] args){
        printContent("args: "+args.toString());
        //init start
        printContent("max memory :"+ Runtime.getRuntime().maxMemory());
        printContent("free memory :"+ Runtime.getRuntime().maxMemory());
        printContent("total memory :"+ Runtime.getRuntime().maxMemory());

        byte[] b1 = new byte[1*1024*1024]; //1* 1024k* 1024字节= 1M兆
        printContent("分配了1M的堆内存");
        printContent("max memory :"+ Runtime.getRuntime().maxMemory());
        printContent("free memory :"+ Runtime.getRuntime().maxMemory());
        printContent("total memory :"+ Runtime.getRuntime().maxMemory());

        printContent("分配了5M的堆内存");
        byte[] b2 = new byte[5*1024*1024];
        printContent("max memory :"+ Runtime.getRuntime().maxMemory());
        printContent("free memory :"+ Runtime.getRuntime().maxMemory());
        printContent("total memory :"+ Runtime.getRuntime().maxMemory());

        int a = 1;
        int b = 2;
        printContent("结果为： " +(b-a)/1024);

    }

    public static void printContent(String content){
        System.out.println(content);
    }

    /**
     /Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/bin/java -XX:+PrintGC -Xms5m -Xmx20m -XX:+UseSerialGC -XX:+PrintGCDetails -XX:+PrintCommandLineFlags "-javaagent:/Applications/IntelliJ IDEA.app/Contents/lib/idea_rt.jar=55611:/Applications/IntelliJ IDEA.app/Contents/bin" -Dfile.encoding=UTF-8 -classpath /Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/charsets.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/deploy.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/ext/cldrdata.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/ext/dnsns.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/ext/jaccess.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/ext/jfxrt.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/ext/localedata.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/ext/nashorn.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/ext/sunec.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/ext/sunjce_provider.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/ext/sunpkcs11.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/ext/zipfs.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/javaws.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/jce.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/jfr.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/jfxswt.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/jsse.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/management-agent.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/plugin.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/resources.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/jre/lib/rt.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/lib/ant-javafx.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/lib/dt.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/lib/javafx-mx.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/lib/jconsole.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/lib/packager.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/lib/sa-jdi.jar:/Library/Java/JavaVirtualMachines/jdk1.8.0_211.jdk/Contents/Home/lib/tools.jar:/Users/apple/IdeaProjects/concurrent/javabase/target/classes:/Users/apple/.m2/repository/com/lmax/disruptor/3.3.7/disruptor-3.3.7.jar:/Users/apple/.m2/repository/com/xuxueli/xxl-crawler/1.2.2/xxl-crawler-1.2.2.jar:/Users/apple/.m2/repository/org/jsoup/jsoup/1.11.2/jsoup-1.11.2.jar:/Users/apple/.m2/repository/org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar:/Users/apple/.m2/repository/io/netty/netty-all/4.1.32.Final/netty-all-4.1.32.Final.jar:/Users/apple/.m2/repository/org/springframework/spring-core/4.2.0.RELEASE/spring-core-4.2.0.RELEASE.jar:/Users/apple/.m2/repository/commons-logging/commons-logging/1.2/commons-logging-1.2.jar:/Users/apple/.m2/repository/commons-lang/commons-lang/2.5/commons-lang-2.5.jar:/Users/apple/.m2/repository/commons-collections/commons-collections/3.2.1/commons-collections-3.2.1.jar:/Users/apple/.m2/repository/junit/junit/4.12/junit-4.12.jar:/Users/apple/.m2/repository/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar:/Users/apple/.m2/repository/org/projectlombok/lombok/1.18.0/lombok-1.18.0.jar:/Users/apple/.m2/repository/com/google/guava/guava/18.0/guava-18.0.jar:/Users/apple/.m2/repository/ch/qos/logback/logback-classic/1.2.1/logback-classic-1.2.1.jar:/Users/apple/.m2/repository/ch/qos/logback/logback-core/1.2.1/logback-core-1.2.1.jar com.sk.jvm.HeapGCParam01 123
     -XX:InitialHeapSize=5242880 -XX:MaxHeapSize=20971520 -XX:+PrintCommandLineFlags -XX:+PrintGC -XX:+PrintGCDetails -XX:+UseCompressedClassPointers -XX:+UseCompressedOops -XX:+UseSerialGC
     args: [Ljava.lang.String;@3fee733d
     max memory :20316160
     free memory :20316160
     total memory :20316160
     [GC (Allocation Failure) [DefNew: 1593K->191K(1856K), 0.0012675 secs] 1593K->461K(5952K), 0.0012869 secs] [Times: user=0.01 sys=0.00, real=0.00 secs]
     分配了1M的堆内存
     max memory :20316160
     free memory :20316160
     total memory :20316160
     分配了5M的堆内存
     [GC (Allocation Failure) [DefNew: 1265K->0K(1856K), 0.0016638 secs][Tenured: 1485K->1486K(4096K), 0.0015156 secs] 1535K->1486K(5952K), [Metaspace: 2937K->2937K(1056768K)], 0.0032401 secs] [Times: user=0.00 sys=0.00, real=0.00 secs]
     max memory :20316160
     free memory :20316160
     total memory :20316160
     结果为： 0
     Heap
     def new generation   total 1920K, used 130K [0x00000007bec00000, 0x00000007bee10000, 0x00000007bf2a0000)
     eden space 1728K,   7% used [0x00000007bec00000, 0x00000007bec20a80, 0x00000007bedb0000)
     from space 192K,   0% used [0x00000007bedb0000, 0x00000007bedb0000, 0x00000007bede0000)
     to   space 192K,   0% used [0x00000007bede0000, 0x00000007bede0000, 0x00000007bee10000)
     tenured generation   total 9220K, used 6606K [0x00000007bf2a0000, 0x00000007bfba1000, 0x00000007c0000000)
     the space 9220K,  71% used [0x00000007bf2a0000, 0x00000007bf913a50, 0x00000007bf913c00, 0x00000007bfba1000)
     Metaspace       used 3026K, capacity 4500K, committed 4864K, reserved 1056768K
     class space    used 329K, capacity 388K, committed 512K, reserved 1048576K

     Process finished with exit code 0

     *
     */
}
