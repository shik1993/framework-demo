package com.sk.jvm;

/**
 * 设置java新生代的相关参数
 * -Xms20m -Xmx20m
 * -Xmn1m新生代大小
 * -XX:SurvivorRatio=2 新生代 eden/s0 或者/s1的 比例
 * -XX:+PrintGCDetails -XX:+UseSerialGC
 * -XX:NewRatio=老年代/新生代比例
 * -XX:PermSize=64M默认的方法区（永久区）大小
 * -XX:MaxPermSize=64M最大的方法区大小
 */
public class HeapNewGenParams{
    /**
     * 第一次配置：什么都不设置，走默认，程序正常退出。
     * 第二次配置：-Xms20m -Xmx20m -Xmn1m -XX:SurvivorRatio=2 -XX:+PrintGCDetails -XX:+UseSerialGC -XX:NewRatio=2
     *              可以看到：新生代总计1m，新生代 eden/s0 或者/s1的 比例=2，老年代约为19M
     *[GC (Allocation Failure)  什么意思？
     *
     *
     */

     public static void main(String[] args){
         byte[] b=null;
         for(int i=0;i<10;i++){
             b = new byte[1*1024*1024];
         }
     }

}
