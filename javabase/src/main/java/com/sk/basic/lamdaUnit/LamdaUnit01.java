package com.sk.basic.lamdaUnit;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.function.Predicate;


/**
 * java 8 lamda表达式学习
 * 01）参数行为化，通过将过滤行为放置到参数列表中，最终达到 代码在灵活性和简洁性之间达到平衡点
 */
public class LamdaUnit01 extends LamdaUnit {

    public static void main(String[] args){
        LamdaUnit lamdaUnit = new LamdaUnit01();
        /**
         * 使用java的提供的默认函数接口 Runnable
         */
        Runnable runnable = () -> {System.out.println("123");};
        runnable.run();
        /**
         * 使用java的提供的默认函数接口 Predicate
         * 让我们利用 lamda和范型 组合来实现 代码简洁性和灵活性之间找到平衡点
         */
        List<Apple> apples = Lists.newArrayList(new Apple(6,"red"),
                new Apple(1,"black"),
                new Apple(7,"black"));
        lamdaUnit.display(apples);
        Predicate<Apple> predicateWight = (Apple apple) -> {return apple.getWight() > 5 ;};
        //第一次尝试
        List<Apple> targetWightAppleList = lamdaUnit.getApple(apples,predicateWight);
        lamdaUnit.display(targetWightAppleList);
        //第二次尝试
        Predicate<Apple> predicateColor = (Apple apple) -> { return apple.getColor().equalsIgnoreCase("red");};
        List<Apple> targetColorAppleList = lamdaUnit.getApple(apples,predicateColor);
        lamdaUnit.display(targetColorAppleList);
    }
}
