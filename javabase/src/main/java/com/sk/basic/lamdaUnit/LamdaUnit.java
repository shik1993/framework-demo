package com.sk.basic.lamdaUnit;

import com.google.common.collect.Lists;
import org.springframework.util.CollectionUtils;

import java.util.Map;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public abstract class LamdaUnit {
    //private static final Logger logger = LoggerFactory.getLogger(LamdaUnit.class);

    /**
     * 参数行为化，通过将过滤行为放置到参数列表中，最终达到 代码在灵活性和简洁性之间达到平衡点
     * @param source
     * @param <T>
     * @return
     */
    public <T> List<T> getApple(List<T> source, Predicate<T> predicate){
        List<T> resultList = Lists.newArrayList();
        for (T s: source) {
            if(predicate.test(s)){
                resultList.add(s);
            }
        }
        return resultList;
    }

    public void displayStr(String source){
         System.out.println(source.toString());
    }


    public  <T> List<T> display(List<T> source){
        if(CollectionUtils.isEmpty(source)){
            return Collections.EMPTY_LIST;
        }
        System.out.println("source size : "+source.stream().count());
        for ( T s: source
             ) {
            System.out.print(s.toString()+"\t");
            System.out.println();
        }

        return source;
    }
}
