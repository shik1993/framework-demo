package com.sk.basic.lamdaUnit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Dish {

    public enum Type{
        MEAT,
        FISH,
        OTHER
    }

    private String name;
    private Boolean vegetarian;
    private Integer calories;
    private Type type;



}
