package com.sk.basic.aboutstatic;

/**
 * 基础类型的包装类的缓冲域问题
 *
 * 1.int和int之间，用==比较，肯定为true。基本数据类型没有equals方法
 2.int和Integer比较，Integer会自动拆箱，调用intValue方法, == 和 equals都肯定为true
 3.int和new Integer比较，Integer会自动拆箱，调用intValue方法, 所以 == 和 equals都肯定为true
 4.Integer和Integer比较的时候，由于直接赋值的话会进行自动的装箱。
 所以当值在[-128,127]中的时候，由于值缓存在IntegerCache中，那么当赋值在这个区间的时候，
 不会创建新的Integer对象，而是直接从缓存中获取已经创建好的Integer对象。而当大于这个区间的时候，会直接new Integer。
 5.当Integer和Integer进行==比较的时候，在[-128,127]区间的时候，为true。不在这个区间，则为false
 6.当Integer和Integer进行equals比较的时候，由于Integer的equals方法进行了重写，比较的是内容，所以为true
 7.Integer和new Integer ： new Integer会创建对象，存储在堆中。而Integer在[-128,127]中，从缓存中取，否则会new Integer.
 所以 Integer和new Integer 进行==比较的话，肯定为false ; Integer和new Integer 进行equals比较的话，肯定为true
 new Integer和new Integer进行==比较的时候，肯定为false ; 进行equals比较的时候，肯定为true
 原因是new的时候，会在堆中创建对象，分配的地址不同，==比较的是内存地址，所以肯定不同
 装箱过程是通过调用包装器的valueOf方法实现的
 拆箱过程是通过调用包装器的xxxValue方法实现的（xxx表示对应的基本数据类型）
 总结：
 包装器的缓存：

 boolean：(全部缓存)
 byte：(全部缓存)
 character(<= 127缓存)
 short(-128 — 127缓存)
 long(-128 — 127缓存)
 integer(-128 — 127缓存)
 float(没有缓存)
 doulbe(没有缓存)
 */
public class AboutInteger {

        public static void main(String[] args) {
            Integer a = 40;
            Integer b = 40;
            Integer c = 0;

            Integer d = new Integer(40);
            Integer e = new Integer(40);
            Integer f = new Integer(0);

            System.out.println(a==b);
            System.out.println(a==b+c);
            System.out.println(d==e);
            System.out.println(d==e+f);
    }
}

/**
 * 第一个：
 * Integer和Integer比较的时候，由于直接赋值的话会进行自动的装箱。
 所以当值在[-128,127]中的时候，由于值缓存在IntegerCache中，那么当赋值在这个区间的时候，不会创建新的Integer对象，而是直接从缓存中获取已经创建好的Integer对象。而当大于这个区间的时候，会直接new Integer。
 *
 * 第二个：
 * b+c会先拆箱，intValue（）然后相加，最后是 int和Integer 对比
 *
 * 第三个：
 * new Integer和new Integer
 *
 * 第四个：
 * 同第二个
 *
 *
 */
