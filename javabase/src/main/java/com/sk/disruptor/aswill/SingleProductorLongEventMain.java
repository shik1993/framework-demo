package com.sk.disruptor.aswill;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *  单生产者，单消费者
 */
public class SingleProductorLongEventMain {

    /**
     * 生产者翻译器
     */
    private static final Translator TRANSLATOR = new Translator();

    public static void main(String[] args) throws Exception {
        // Executor that will be used to construct new threads for consumers
        // 废弃
        ExecutorService executor = Executors.newCachedThreadPool();

        ThreadFactory threadFactory = new ThreadFactory() {

            private final AtomicInteger index = new AtomicInteger(1);

            //@Override
            public Thread newThread(Runnable r) {
                return new Thread((ThreadGroup) null, r, "disruptor-thread-" + index.getAndIncrement());
            }
        };

        // Specify the size of the ring buffer, must be power of 2.
        /**
         * ringBuffer的大小，规定必须是2的倍数
         *
         */
        int bufferSize = 1024;

        // 创建 Disruptor
        Disruptor<LongEvent> disruptor = new Disruptor<LongEvent>(new EventFactory(){
            public LongEvent newInstance() {
//                LongEvent longEvent = new LongEvent();
//                longEvent.set(1001L);
                return new LongEvent();
            }
        }, bufferSize, threadFactory, ProducerType.SINGLE, new BlockingWaitStrategy());

        // 创建消费者，并且将disruptor 连接 handler
        disruptor.handleEventsWith(new LongEventHandler());

        // 启动disruptor
        disruptor.start();

        // 获取ringBuffer
        RingBuffer<LongEvent> ringBuffer = disruptor.getRingBuffer();

        ByteBuffer bb = ByteBuffer.allocate(8);
        for (long l = 0; l < 10; l++) {
            bb.putLong(0, l); //将数据l放置在byte的第一个位置
            System.out.println("生产者生产消息并放置发布到ringBuffer中" + bb.getLong(0));
            ringBuffer.publishEvent(TRANSLATOR, bb);
            Thread.sleep(1000L);
        }

        disruptor.shutdown();
    }
}
