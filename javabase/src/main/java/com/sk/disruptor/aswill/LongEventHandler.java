package com.sk.disruptor.aswill;

import com.lmax.disruptor.EventHandler;

/**
 * 一个消息的消费者
 */
public class LongEventHandler implements EventHandler<LongEvent> {

    public void onEvent(LongEvent event, long sequence, boolean endOfBatch) {
        System.out.println(
                " \t流程执行到消费者：Event: "
                + event.getValue() +
                " \tThreadName: " +
                Thread.currentThread().getName() +
                " \tsequence :" +
                sequence +
                " \tendOfBatch: " +
                endOfBatch
        );
    }
}
