package com.sk.concurrent.multi01;

import org.junit.Test;

/**
 * 线程安全概念：当多个线程访问某一个类（对象或方法）时，这个对象始终都能表现出正确的行为，那么这个类（对象或方法）就是线程安全的。
 * synchronized：可以在任意对象及方法上加锁，而加锁的这段代码称为"互斥区"或"临界区"
 * 未加锁synchronized时，会出现错误的结果，因为cpu的执行顺序不同
 */
public class MyThread001 extends Thread{
	
	private int count = 50 ;
	
	//synchronized加锁
	 public synchronized void run(){
	//public void run(){
		count--;
		System.out.println(this.currentThread().getName() + " count = "+ count);
	}

	/**
	 * 说明：
	 * 比如说有一个类  MyThread001的对象去做一个功能点： public void run() {--操作}
	 * 现在同时开启50个线程去处理这件事，如果不加锁的话，会出现错误的结果，既：
	 * 当多个线程访问myThread001的run方法时，以排队的方式进行处理（这里排对是按照CPU分配的先后顺序而定的）
	 * thread34 count = 8
	 thread30 count = 7
	 thread26 count = 6
	 thread22 count = 5
	 目前执行到的 currentThread :main
	 thread18 count = 4
	 thread14 count = 3
	 thread10 count = 2
	 thread6 count = 1
	 thread2 count = 0
	 thread49 count = 9

	 将thread的执行方法改为: public synchronized void run() 后的结果 ：
	 thread43 count = 10
	 thread39 count = 9
	 thread35 count = 8
	 thread31 count = 7
	 thread27 count = 6
	 thread23 count = 5
	 thread19 count = 4
	 thread15 count = 3
	 thread11 count = 2
	 thread7 count = 1
	 thread3 count = 0

	 */
	@Test
	public void testWithoutSync() {
		/**
		 * 分析：当多个线程访问myThread的run方法时，以排队的方式进行处理（这里排对是按照CPU分配的先后顺序而定的），
		 * 		一个线程想要执行synchronized修饰的方法里的代码：
		 * 		1 尝试获得锁
		 * 		2 如果拿到锁，执行synchronized代码体内容；拿不到锁，这个线程就会不断的尝试获得这把锁，直到拿到为止，
		 * 		   而且是多个线程同时去竞争这把锁。（也就是会有锁竞争的问题）
		 * 	本方法执行后， 同时开启了 51个线程：  50个线程+1个主线程
		 */
		MyThread001 myThread = new MyThread001();
		for(int i = 0 ; i <50 ; i ++){
			Thread t = new Thread(myThread,"thread"+i);
			t.start();
		}
		System.out.println("目前执行到的 currentThread :" + Thread.currentThread().getName() );

		try {
			Thread.sleep(3000);//让线程等待3秒钟，防止junit自己断了
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}












