package com.sk.concurrent.multi01;
/**
 * 锁的重入
 * synchronized的重入
 * 同一个线程，对于同一个对象内的多个同步方法，可以串行重入执行。
 * @author alienware
 *
 */
public class SyncDubbo1 {

	public synchronized void method1(){
		System.out.println("method1.. "+Thread.currentThread().getName());
		method2();
	}
	public synchronized void method2(){
		System.out.println("method2.. "+Thread.currentThread().getName());
		method3();
	}
	public synchronized void method3(){
		System.out.println("method3.. "+Thread.currentThread().getName());
	}
	
	public static void main(String[] args) {
		final SyncDubbo1 sd = new SyncDubbo1();
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				sd.method1();
			}
		});
		t1.start();

		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				sd.method1();
			}
		});
		t2.start();
	}
}
