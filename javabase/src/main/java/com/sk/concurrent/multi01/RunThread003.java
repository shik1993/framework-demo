package com.sk.concurrent.multi01;

/**
 * volatile关键字 可以保证 线程之间的可见性，
 * 做法就是：强制线程在读取volatile关键字修饰的变量时，一定要从 主内存中获取，不得使用线程工作区的变量值
 * 从而做到了 线程的可见性， 可以理解为 轻量级synchronized , netty框架就大量的使用到了  volitile
 */
public class RunThread003 extends Thread{

    /**
     * 当使用这行代码时,更新的isRunning是主内存中的,工作内存中的isRunning不受影响
	 */
	//private  boolean isRunning = true;

    /**
     * 当使用这行代码时,volatile关键字强行要求线程从主内存中去读取isRunning字段
	 * @param isRunning
	 */
	private volatile boolean isRunning = true;

	private void setRunning(boolean isRunning){
		this.isRunning = isRunning;
	}
	
	public void run(){
		System.out.println("进入run方法..");
		int i = 0;
		while(isRunning == true){
			//..
		}
		System.out.println("线程停止");
	}
	
	public static void main(String[] args) throws InterruptedException {
		RunThread003 rt = new RunThread003();
		rt.start();
		Thread.sleep(1500);
		rt.setRunning(false);
		System.out.println("isRunning的值已经被设置了false");
	}
	
	
}
