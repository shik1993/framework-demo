package com.sk.concurrent.multi01;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 技术类的问题
 * volatile关键字不具备synchronized关键字的原子性（同步）
 * 如果count为 volatile类型时，如果同时开启50个线程时，最终会出现
 33300
 34200
 35100
 36170
 36268
 38068
 39868
 40768
 41668
 42763
 37168
 43357
 38968
 44257
 这样子错误的结果，多线程情况下，如果是50个线程，每次加900，最终结果为45000
 所以这个时候我们需要将 volatile修饰的 count改成Atomic包下的修饰，
 就可以解决这一个问题
 */
public class VolatileNoAtomic extends Thread{
	//private static volatile int count;
	private static AtomicInteger count = new AtomicInteger(0);
	private static void addCount(){
		for (int i = 0; i < 900; i++) {
			//count++ ;
			count.incrementAndGet();
		}
		System.out.println(count);
	}
	
	public void run(){
		addCount();
	}
	
	public static void main(String[] args) {
		
		VolatileNoAtomic[] arr = new VolatileNoAtomic[100];
		for (int i = 0; i < 50; i++) {
			arr[i] = new VolatileNoAtomic();
		}
		
		for (int i = 0; i < 50; i++) {
			arr[i].start();
		}
	}
	
	
	
	
}
