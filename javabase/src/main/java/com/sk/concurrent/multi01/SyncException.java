package com.sk.concurrent.multi01;

import java.util.ArrayList;
import java.util.List;

/**
 * synchronized异常
 * @author alienware
 *
 * 当执行到第十条,发现报错,此时t1线程的锁对象被释放,后续t1会继续执行i11,此时如果i10步骤没有成功提交,那么肯定是有问题的
 * 解决方案: 1;
 * catch改为
 * catch (InterruptedException ie){
ie.printStackTrace();
}
 *
 * 或者在 catch中抛出throw new RuntimeException()
 */
public class SyncException {

	private int i = 1000;
	public synchronized void operation(){
		while(true){
			try {
				i--;
				Thread.sleep(100);
				System.out.println(Thread.currentThread().getName() + " , i = " + i);
				if(i >= 888 && i <1000){
					Integer.parseInt("a");
					throw new Exception();
				}
			}catch (Exception e) {
				e.printStackTrace();
				System.out.println("logger.info " + Thread.currentThread().getName() + " , i = " + i);
			}
		}
	}

	public static void main(String[] args) {
		List<Thread> ts = new ArrayList<Thread>();
		final SyncException se = new SyncException();
		for (int i = 0; i < 100; i++) {
			ts.add(new Thread(new Runnable() {
				@Override
				public void run() {
					se.operation();
				}
			}));
		}

		for(Thread t : ts){
			t.start();
		}
	}


}