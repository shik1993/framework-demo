package com.sk.concurrent.multi01;

/**
 * 使用synchronized代码块加锁,比较灵活
 * 从这个例子可以看出来
 * 1.主线程会一直跑下去(先打印 		System.out.println(); ),其他线程按照顺序执行自己的.
 * 2.类锁和对象锁，任何对象锁  是三把不同的锁，不相互冲突,可以同时执行，因为所得内容不同
 * 本main执行后， 同时开启了 6个线程：  5个线程+1个主线程
 *
 *
 */
public class ObjectLock002 {

	public void method1(){
		synchronized (this) {	//对象锁
			try {
				System.out.println("do method1..");
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void method2(){		//类锁
		synchronized (ObjectLock002.class) {
			try {
				System.out.println("do method2..");
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private Object lock = new Object();
	public void method3(){		//任何对象锁
		synchronized (lock) {
			try {
				System.out.println("do method3..");
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static void main(String[] args) {
		
		final ObjectLock002 objLock = new ObjectLock002();
		Thread t1 = new Thread( new Runnable() {
			@Override
			public void run() {
				objLock.method1();
			}
		});
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				objLock.method2();
			}
		});
		Thread t3 = new Thread(new Runnable() {
			@Override
			public void run() {
				objLock.method3();
			}
		});
		Thread t4 = new Thread(new Runnable() {
			@Override
			public void run() {
				objLock.method2();
			}
		});
		Thread t5 = new Thread(new Runnable() {
			@Override
			public void run() {
				objLock.method1();
			}
		});
		
		t1.start();
		t2.start();
		t3.start();
		System.out.println("目前执行到的 currentThread :" + Thread.currentThread().getName() );
		t4.start();
		t5.start();
		
	}
	
}
