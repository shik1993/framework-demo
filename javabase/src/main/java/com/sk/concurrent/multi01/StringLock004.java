package com.sk.concurrent.multi01;
/**
 * synchronized代码块对字符串的锁，注意String常量池的缓存功能
 * 可能会造成多个线程持有 相同的一把锁，所以一般避免使用 字符串对象 制作的锁，
 * 执行结果如下:
 当前线程 : t1开始
 当前线程 : t1结束
 当前线程 : t1开始
 当前线程 : t1结束
 当前线程 : t1开始
 当前线程 : t1结束
 当前线程 : t1开始
 当前线程 : t1结束
 当前线程 : t1开始

 本方法执行后，一共有 4个线程开启
 "字符串常量" 和 new String("字符串常量") 是 同一个对象 （String常量池的缓存功能）
 造成 thread1永远持有这把锁，并永远执行， 而  thread2 无法执行
 *
 */
public class StringLock004 {

	public void method() {
		//new String("字符串常量")
		synchronized ("字符串常量") {
			try {
				while(true){
					System.out.println("当前线程 : "  + Thread.currentThread().getName() + "开始");
					Thread.sleep(1000);		
					System.out.println("当前线程 : "  + Thread.currentThread().getName() + "结束");
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void method2() {
		//new String("字符串常量")
		synchronized (new String("字符串常量")) {
			try {
				while(true){
					System.out.println("当前线程 : "  + Thread.currentThread().getName() + "开始");
					Thread.sleep(1000);
					System.out.println("当前线程 : "  + Thread.currentThread().getName() + "结束");
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		final StringLock004 stringLock = new StringLock004();
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				stringLock.method();
			}
		},"t1");
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				stringLock.method();
			}
		},"t2");
		Thread t3 = new Thread(new Runnable() {
			@Override
			public void run() {
				stringLock.method();
			}
		},"t3");
		
		t1.start();
		t2.start();
		t3.start();
	}
}
