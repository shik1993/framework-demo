package com.sk.concurrent.multi01;

/**
 * 对象锁的同步和异步问题
 * 本例子MyObject和Optimize的区别：
 * Optimize：当对一个方法中的代码块（A）加锁时，A以上的代码部分是可以被持有同样对象的多个线程都执行的。
 * MyObject：
 *  t1线程先持有object对象的Lock锁，t2线程可以以异步的方式调用对象中的非synchronized修饰的方法
 *  t1线程先持有object对象的Lock锁，t2线程如果在这个时候调用对象中的同步（synchronized）方法则需等待，也就是同步
 *  从上述例子总结：
 *  多个线程并行执行同一个对象中的多个功能块A（同步方法，非同步方法，同步代码块，非同步代码块)时。
 *  同步A被一个线程调用时，其他线程只可以调用非同步A，如果要调用A则需要等待。
 *  再结合 SyncDubbo1和SyncDubbo2 锁的可重入性：
 *  多个线程并行执行同一个对象中的多个功能块A（同步方法，非同步方法，同步代码块，非同步代码块)时，
 *  同一个线程可以同时串行可重入与多个同步A中，只要该线程持有的是同一个锁对象。
 *
 */
public class MyObject {

	public synchronized void method1(){
		try {
			System.out.println(Thread.currentThread().getName());
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/** synchronized */
	/*
	public synchronized void method2(){
			System.out.println(Thread.currentThread().getName());
	}
	*/
	public void method2(){
		try{
		System.out.println(Thread.currentThread().getName());
		Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		
		final MyObject mo = new MyObject();
		
		/**
		 * 分析：
		 * t1线程先持有object对象的Lock锁，t2线程可以以异步的方式调用对象中的非synchronized修饰的方法
		 * t1线程先持有object对象的Lock锁，t2线程如果在这个时候调用对象中的同步（synchronized）方法则需等待，也就是同步
		 */
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				mo.method1();
			}
		},"t1");
		
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				mo.method2();
			}
		},"t2");
		
		t1.start();
		t2.start();
		
	}
	
}
