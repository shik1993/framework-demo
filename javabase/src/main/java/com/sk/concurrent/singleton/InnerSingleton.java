package com.sk.concurrent.singleton;

/**
 * 单例模式： 饿汉式,类初始化时直接创建
 */
public class InnerSingleton {
	
	private static class Singletion {
		private static Singletion single = new Singletion();
	}
	
	public static Singletion getInstance(){
		return Singletion.single;
	}
	
}
