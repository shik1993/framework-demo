package com.sk.concurrent.singleton;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * 测试懒汉式的问题代码
 * 懒汉：需要时创建新对象，启动100个线程加载
 * 以下为正确的case
 */
public class DubbleSingletonRightCase {

	private static DubbleSingletonRightCase ds;
	
	public static DubbleSingletonRightCase getDs(){
		if(ds == null){
			try {
				//模拟初始化对象的准备时间...
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			synchronized (DubbleSingletonRightCase.class) {
				if(ds == null){
					ds = new DubbleSingletonRightCase();
				}
			}
		}
		return ds;
	}
	
	public static void main(String[] args) {
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println(DubbleSingletonRightCase.getDs().hashCode());
			}
		},"t1");
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println(DubbleSingletonRightCase.getDs().hashCode());
			}
		},"t2");
		Thread t3 = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println(DubbleSingletonRightCase.getDs().hashCode());
			}
		},"t3");

		Map<Integer,Thread> threadMap = Maps.newConcurrentMap();
		for(int i=4;i<100;i++){
			threadMap.put(i,new Thread(new Runnable() {
				@Override
				public void run() {
					System.out.println(DubbleSingletonRightCase.getDs().hashCode());
				}
			},"t"+i));
		}
		for(Thread threadTemp : threadMap.values()){
			threadTemp.start();
		}
		
		t1.start();
		t2.start();
		t3.start();
	}
	
}
