package com.sk.concurrent.executor.executorBySelf;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.DiscardOldestPolicy;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

/**
 * 自定义线程池1
 */
public class UseThreadPoolExecutor1 {

	/**
	 * 	 六个线程一起执行的结果如下：
	 *
	 自定义处理..
	 当前被拒绝任务为：6
	 run taskId =1,currentThreadName = pool-1-thread-1
	 run taskId =5,currentThreadName = pool-1-thread-2
	 run taskId =2,currentThreadName = pool-1-thread-1
	 run taskId =3,currentThreadName = pool-1-thread-2
	 run taskId =4,currentThreadName = pool-1-thread-2
	 * @param args
	 */
	public static void main(String[] args) {
		/**
		 * 在使用有界队列时，若有新的任务需要执行，如果线程池实际线程数小于corePoolSize，则优先创建线程，
		 * 若大于corePoolSize，则会将任务加入队列，
		 * 若队列已满，则在总线程数不大于maximumPoolSize的前提下，创建新的线程，
		 * （这里的总线程输指的是线程池中实际在跑的线程数（max线程数=核心线程数+可新增线程））
		 * （下面的demo(以6个线程加入线程池为例子 )可以尝试将maxSize先改成2，会触发拒绝策略，再改成3，不会触发拒绝策略）
		 * 若线程数大于maximumPoolSize，则执行拒绝策略。或其他自定义方式。
		 * 通过程序运行可以发现：当maxSize设置为2，总任务为6，核心线程池为1，运行一个，队列为2，进来两个，
		 * 还有两个，生成一个新的线程，1+1=maxSize=2，最后留一个 走拒绝策略
		 * 
		 */	
		ThreadPoolExecutor pool = new ThreadPoolExecutor(
				1, 				//coreSize
				2, 				//MaxSize
				60, 			//60
				TimeUnit.SECONDS, 
				new ArrayBlockingQueue<Runnable>(3)			//指定一种队列 （有界队列）
				//new LinkedBlockingQueue<Runnable>()
				, new MyRejected()
				//, new DiscardOldestPolicy()
				);
		
		MyTask mt1 = new MyTask(1, "任务1");
		MyTask mt2 = new MyTask(2, "任务2");
		MyTask mt3 = new MyTask(3, "任务3");
		MyTask mt4 = new MyTask(4, "任务4");
		MyTask mt5 = new MyTask(5, "任务5");
		MyTask mt6 = new MyTask(6, "任务6");
		
		pool.execute(mt1);
		pool.execute(mt2);
		pool.execute(mt3);
		pool.execute(mt4);
		pool.execute(mt5);
		pool.execute(mt6);
		
		pool.shutdown();
	}
}
