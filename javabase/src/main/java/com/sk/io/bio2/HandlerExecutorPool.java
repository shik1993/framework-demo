package com.sk.io.bio2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 自定义线程池对象
 * 传统的 server / client 模式会基于 TPR ( Thread per Request ) ．
 * 服务器会为每个客户端请求建立一个线程．由该线程单独负贵处理一个客户请求。
 * 这种模式带未的一个问题就是线程数是的剧增．大量的线程会增大服务器的开销，
 * 大多数的实现为了避免这个问题，都采用了线程池模型，并设置线程池线程的最大数量，
 * 这又带来了新的问题，如果线程池中有 200 个线程，而有 200 个用户都在进行大文件下载，
 * 会导致第 201 个用户的请求无法及时处理，即便第 201 个用户只想请求一个几 KB 大小的页面。
 */
public class HandlerExecutorPool {

	private ExecutorService executor;
	public HandlerExecutorPool(int maxPoolSize, int queueSize){
		this.executor = new ThreadPoolExecutor(
				Runtime.getRuntime().availableProcessors(),
				maxPoolSize, 
				120L, 
				TimeUnit.SECONDS,
				new ArrayBlockingQueue<Runnable>(queueSize));
	}
	
	public void execute(Runnable task){
		this.executor.execute(task);
	}
	
	
	
}
