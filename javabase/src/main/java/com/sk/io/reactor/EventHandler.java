package com.sk.io.reactor;

/**
 * @Description: event处理器的抽象类
 * @Modify by:
 */
public abstract class EventHandler {

    private InputSource source;
    public abstract void handle(Event event);

    public InputSource getSource() {
        return source;
    }

    public void setSource(InputSource source) {
        this.source = source;
    }
}
