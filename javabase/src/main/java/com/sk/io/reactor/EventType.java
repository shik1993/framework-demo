package com.sk.io.reactor;

/**
 * EventType: 枚举类型表示事件的不同类型
 * 事件类型
 * Created by shik on 2019/4/16.
 */
public enum EventType {
    ACCEPT,
    READ,
    WRITE;
}
