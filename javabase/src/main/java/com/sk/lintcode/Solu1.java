package com.sk.lintcode;


import java.util.HashSet;
import java.util.Set;

/**
 *
 字符串压缩。利用字符重复出现的次数，编写一种方法，实现基本的字符串压缩功能。比如，字符串aabcccccaaa会变为a2b1c5a3。若“压缩”后的字符串没有变短，则返回原先的字符串。你可以假设字符串中只包含大小写英文字母（a至z）。

 示例1:

 输入："aabcccccaaa"
 输出："a2b1c5a3"
 示例2:

 输入："abbccd"
 输出："abbccd"
 解释："abbccd"压缩后为"a1b2c2d1"，比原字符串长度更长。
 提示：

 字符串长度在[0, 50000]范围内。

 *
 *
 */
public class Solu1 {
    public String compressString(String S) {
        int len = S.length();
        if (len == 0) {
            return "";
        }

        char[] chars = S.toCharArray();
        StringBuilder stringBuilder = new StringBuilder(chars[0]);

        int index = 0;
        int cnt = 1;
        for (int i = 1; i < len; i++) {
            if (chars[i] == chars[index]) {
                cnt++;
            } else {
                stringBuilder.append(chars[index]);
                stringBuilder.append(cnt);
                index = i;
                cnt = 1;
            }
        }

        stringBuilder.append(chars[len - 1]);
        stringBuilder.append(cnt);

        String res = stringBuilder.toString();
        if (res.length() >= len){
            return S;
        }
        return res;
    }

    public static void main(String[] args) {
        Solu1 solution = new Solu1();
        String S = "aabcccccaaaaaaaa";
        String res = solution.compressString(S);
        System.out.println(res);
        String res2 = solution.compressStr(S);
        System.out.print(res2);
    }


    public String compressStr(String str){
        int length =str.length();

        char[] charArray = str.toCharArray();
        int index = 0;
        int cnt = 1 ;
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 1 ; i < length;i++){
            if(charArray[index] == charArray[i]){
                cnt++;
            }else {
                stringBuilder.append(charArray[index]);
                stringBuilder.append(cnt);
                index = i;
                cnt = 1;
            }
        }
        stringBuilder.append(charArray[length - 1]);
        stringBuilder.append(cnt);

        String newStr = stringBuilder.toString();
        if(newStr.length() >= str.length()){
            return str;
        }else {
            return newStr;
        }
    }
}
