package com.sk.lintcode.sort;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。
 *
 * 示例 1:
 *
 * 输入: s = "anagram", t = "nagaram"
 * 输出: true
 * 示例 2:
 *
 * 输入: s = "rat", t = "car"
 * 输出: false
 * 说明:
 * 你可以假设字符串只包含小写字母。
 *
 * 进阶:
 * 如果输入字符串包含 unicode 字符怎么办？你能否调整你的解法来应对这种情况？
 *
 */
public class Solu5 {

    /**
     * 我写的
     * @param s
     * @param t
     * @return
     */
    public boolean isAnagram_me(String s,String t){
        if(s == null && t == null){
            return true;
        }
        if(s == null || t == null){
            return false;
        }
        if(s.length() != t.length()){
            return false;
        }
        char[] s1Char = s.toCharArray();
        char[] s2Char = t.toCharArray();
        Map<Character,Integer> countMap = new HashMap<>();
        for(int i = 0 ; i < s1Char.length ; i++){
            Character character = s1Char[i];
            countMap.put(character,countMap.getOrDefault(character,0)+1);
        }

        for(int i = 0 ; i < s2Char.length ; i++){
            Character character = s2Char[i];
            if(countMap.isEmpty() || countMap.get(character) == null){
                return false;
            }else if(countMap.get(character) > 1){
                countMap.put(character,countMap.get(character) - 1);
            }else{
                countMap.remove(character);
            }
        }

        return countMap.isEmpty();
    }

    public static void main(String[] args){
        Solu5 solu5 = new Solu5();
        System.out.println(solu5.isAnagram("anagram","nagaram"));
        System.out.println(solu5.isAnagram("rat","car"));
    }


    public boolean isAnagram_1(String s, String t) {
        char[] sChars = s.toCharArray();
        char[] tChars = t.toCharArray();
        Arrays.sort(sChars);
        Arrays.sort(tChars);
        return String.valueOf(sChars).equals(String.valueOf(tChars));
    }

    public boolean isAnagram_2(String s, String t) {
        Map<Character, Integer> map = new HashMap<>();
        for (char ch : s.toCharArray()) {
            map.put(ch, map.getOrDefault(ch, 0) + 1);
        }
        for (char ch : t.toCharArray()) {
            Integer count = map.get(ch);
            if (count == null) {
                return false;
            } else if (count > 1) {
                map.put(ch, count - 1);
            } else {
                map.remove(ch);
            }
        }
        return map.isEmpty();
    }

    public boolean isAnagram(String s, String t) {
        int[] sCounts = new int[26];
        int[] tCounts = new int[26];
        for (char ch : s.toCharArray()) {
            sCounts[ch - 'a']++;
        }
        for (char ch : t.toCharArray()) {
            tCounts[ch - 'a']++;
        }
        for (int i = 0; i < 26; i++) {
            if (sCounts[i] != tCounts[i]) {
                return false;
            }
        }
        return true;
    }

}
