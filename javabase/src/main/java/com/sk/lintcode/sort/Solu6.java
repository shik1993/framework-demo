package com.sk.lintcode.sort;

import java.util.*;

/**
 * 给定两个数组，编写一个函数来计算它们的交集。
 *
 * 示例 1:
 *
 * 输入: nums1 = [1,2,2,1], nums2 = [2,2]
 * 输出: [2,2]
 * 示例 2:
 *
 * 输入: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
 * 输出: [4,9]
 * 说明：
 *
 * 输出结果中每个元素出现的次数，应与元素在两个数组中出现的次数一致。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/intersection-of-two-arrays-ii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solu6 {

    public int[] intersect_me(int[] nums1,int[] nums2){
        int size1 = nums1.length;
        int size2 = nums2.length;

        Map<Integer,Integer> map = new HashMap<>();
        Map<Integer,Integer> resultMap = new HashMap<>();
        for(int i = 0 ; i < size1;i++){
            if(!map.containsKey(nums1[i])){
                map.put(nums1[i],1);
            }else {
                map.put(nums1[i], map.get(nums1[i])+ 1);
            }
        }
        for(int j=0;j<size2;j++){
            if(!map.containsKey(nums2[j])){
                continue;
            }
            if(map.isEmpty()){
                return new int[]{};
            }else if(map.get(nums2[j]) > 0 && map.get(nums2[j]) >resultMap.getOrDefault(nums2[j],0)){
                resultMap.put(nums2[j],resultMap.getOrDefault(nums2[j],0)+1);
            }else{
            }
        }


        int index = 0;
        int size = 0;
        for(int c : resultMap.values()){
            size += c;
        }
        int[] result =new int[size];
        Set<Map.Entry<Integer,Integer>> entrySet = resultMap.entrySet();
        for(Map.Entry<Integer,Integer> entry : entrySet){
            for(int k = 0; k< entry.getValue();k++){
                result[index] = entry.getKey();
                index++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Solu6 solu6 = new Solu6();
        int[] result = solu6.intersect(new int[]{1, 2, 2, 1}, new int[]{2, 2});
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
        }

        int[] result2 = solu6.intersect(new int[]{4, 9, 5}, new int[]{9, 4, 9, 8, 4});
        for (int i = 0; i < result2.length; i++) {
            System.out.println(result2[i]);
        }
    }

        /**
         * 排序预处理
         */
        public int[] intersect(int[] nums1, int[] nums2) {
            Arrays.sort(nums1);
            Arrays.sort(nums2);
            List<Integer> list = new ArrayList<>();
            for (int i = 0, j = 0; i < nums1.length && j < nums2.length; ) {
                if (nums1[i] < nums2[j]) {
                    i++;
                } else if (nums1[i] > nums2[j]) {
                    j++;
                } else {
                    list.add(nums1[i]);
                    i++;
                    j++;
                }
            }
            int[] res = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                res[i] = list.get(i);
            }
            return res;
        }
    }
