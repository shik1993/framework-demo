package com.sk.lintcode.easy;

//编写一个函数来查找字符串数组中的最长公共前缀。
//
// 如果不存在公共前缀，返回空字符串 ""。
//
// 示例 1:
//
// 输入: ["flower","flow","flight"]
//输出: "fl"
//
//
// 示例 2:
//
// 输入: ["dog","racecar","car"]
//输出: ""
//解释: 输入不存在公共前缀。
//
//
// 说明:
//
// 所有输入只包含小写字母 a-z 。
// Related Topics 字符串
// 👍 1364 👎 0


import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

//leetcode submit region begin(Prohibit modification and deletion)
class _最长公共前缀 {
    public String longestCommonPrefix(String[] strs) {
        String result = "";
        if(strs == null || strs.length == 0){
            return result;
        }
        if( strs.length ==1 ){
            return strs[0];
        }
        result = strs[0];
        for(String str : strs){
            if(result.equals("")){
                return result;
            }
            while (!str.startsWith(result)){
                result = result.substring(0,result.length()-1);
            }
        }
        return result;
    }

    public static void main(String[] args){
        _最长公共前缀 solution = new _最长公共前缀();
        System.out.println("result : "+solution.longestCommonPrefix(new String[]{"flower","flow","flight"}));
        System.out.println("result : "+solution.longestCommonPrefix(new String[]{"a"}));
        System.out.println("result : "+solution.longestCommonPrefix(new String[]{}));
        System.out.println("result : "+"a".substring(0,0));
    }
}
//leetcode submit region end(Prohibit modification and deletion)
