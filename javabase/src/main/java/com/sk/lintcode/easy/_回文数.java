package com.sk.lintcode.easy;

//判断一个整数是否是回文数。回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。
//
// 示例 1:
//
// 输入: 121
//输出: true
//
//
// 示例 2:
//
// 输入: -121
//输出: false
//解释: 从左向右读, 为 -121 。 从右向左读, 为 121- 。因此它不是一个回文数。
//
//
// 示例 3:
//
// 输入: 10
//输出: false
//解释: 从右向左读, 为 01 。因此它不是一个回文数。
//
//
// 进阶:
//
// 你能不将整数转为字符串来解决这个问题吗？
// Related Topics 数学
// 👍 1321 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
class _回文数 {

    /**
     * 官方解答
     */
    public boolean isPalindrome2(int x) {
        if( x < 0 ){
            return Boolean.FALSE;
        }
        int cur = 0;
        int num = x;
        while (num != 0){
            cur = cur * 10 + num % 10;
            num /= 10;
        }
        return cur == num;
    }

    /**
     * 我的做法
     * @param x
     * @return
     */
    public boolean isPalindrome(int x) {
        if(x < 0){
            return Boolean.FALSE;
        }
        if(x >=0 && x<10){
            return Boolean.TRUE;
        }
        String value = String.valueOf(x);
        char[] value_array = value.toCharArray();
        for(int i = 0;i <value_array.length;i++ ){
            if(!String.valueOf(value_array[i]).equalsIgnoreCase(String.valueOf(value_array[value_array.length-i-1]))){
                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    public static  void main(String[] args){
        int value = -191;
        _回文数 solution = new _回文数();
        System.out.println(solution.isPalindrome(value));
    }
}
//leetcode submit region end(Prohibit modification and deletion)

