package com.sk.lintcode.easy;

import com.sk.lintcode.array.ListNode;

import java.util.Stack;

public class _非空链表求和 {

    /**
     * 给你两个非空链表, 他们每位 数字都在按照逆序 存储,且每个节点都只能存储1位数字
     * 请将两个数相加, 并以相同方式 返回一个表示 和的链表
     * 除了0以外, 这两个数都不会以0开头
     *
     * in L1= [2,4,3]   L2 = [5,6,4]
     * out [708]
     *
     * in L1=[0]  L2 = [0]
     * out [0]
     *
     * in L1=[9,9,9,9,9,9,9] , L2=[9,9,9,9]
     * out [8,9,9,9,0,0,0,1]
     *
     */

    public static void main(String[] args){
        //数组的形式
//        int[] L1 = new int[]{2,4,3};
//        int[] L2 = new int[]{5,6,4};
//        int[] L1 = new int[]{0};
//        int[] L2 = new int[]{0};
//        int[] L1 = new int[]{9,9,9,9,9,9,9};
//        int[] L2 = new int[]{9,9,9,9};
//
//        String result = execResult(L1 , L2);
//        System.out.println("lastest "+ result );

        //链表的形式
//        ListNode listNode0 = new ListNode(2);
//        ListNode listNode1 = new ListNode(4);
//        ListNode listNode2 = new ListNode(3);
//
//        ListNode listNode3 = new ListNode(5);
//        ListNode listNode4 = new ListNode(6);
//        ListNode listNode5 = new ListNode(4);
//
//        listNode2.setNext(null);
//        listNode1.setNext(listNode2);
//        listNode0.setNext(listNode1);
//
//        listNode5.setNext(null);
//        listNode4.setNext(listNode5);
//        listNode3.setNext(listNode4);


//        ListNode listNode0 = new ListNode(0);
//        ListNode listNode3 = new ListNode(0);


                ListNode listNode0 = new ListNode(9);
        ListNode listNode1 = new ListNode(9);
        ListNode listNode2 = new ListNode(9);
        ListNode listNode7 = new ListNode(9);
        ListNode listNode8 = new ListNode(9);
        ListNode listNode9 = new ListNode(9);
        ListNode listNode10 = new ListNode(9);


        ListNode listNode3 = new ListNode(9);
        ListNode listNode4 = new ListNode(9);
        ListNode listNode5 = new ListNode(9);
        ListNode listNode6 = new ListNode(9);

        listNode10.setNext(null);
        listNode9.setNext(listNode10);
        listNode8.setNext(listNode9);
        listNode7.setNext(listNode8);
        listNode2.setNext(listNode7);
        listNode1.setNext(listNode2);
        listNode0.setNext(listNode1);

        listNode6.setNext(null);
        listNode5.setNext(listNode6);
        listNode4.setNext(listNode5);
        listNode3.setNext(listNode4);

        ListNode.printListNode(listNode0);
        System.out.println("----------------------");
        ListNode.printListNode(listNode3);
        System.out.println("----------------------");
        ListNode result = execResult(listNode0,listNode3);
        ListNode.printListNode(result);
    }


    /**
     * 暴力解法 , 入参为链表的形式
     * @param
     * @param
     * @return
     */
    public static ListNode execResult(ListNode node1,ListNode node2){
        ListNode pre = new ListNode(0);
        ListNode cur = pre;

        int carry = 0;//进一位
        while (node1 != null || node2 != null) {
            int node1V = node1 == null ? 0 : node1.getValues();
            int node2V = node2 == null ? 0 : node2.getValues();
            int sum = node1V + node2V + carry ;
            carry = sum / 10;
            int temp = sum % 10;

            cur.setNext(new ListNode(temp));
            cur = cur.getNext();
            if(node1 != null)
                node1 = node1.getNext();
            if(node2 != null)
                node2 = node2.getNext();
        }
        if(carry > 0){
            cur.setNext(new ListNode(carry));
        }

        return pre.getNext();

    }

    /**
     * 暴力解法 , 入参为数组的形式
     * @param l1
     * @param l2
     * @return
     */
    private static String execResult(int[] l1, int[] l2) {
        if(l1 == null || l2 == null){
            throw new RuntimeException("l1 || l2 is null");
        }
        Stack<Integer> stack = new Stack<>();
        for(int i = 0;i<l1.length;i++){
            stack.push(l1[i]);
        }
        String liStr = "";
        while (!stack.empty()) {
            liStr += stack.pop();
        }
        for(int i = 0;i<l2.length;i++){
            stack.push(l2[i]);
        }

        String l2Str = "";
        while (!stack.empty()) {
            l2Str += stack.pop();
        }
        System.out.println(liStr +" , "+l2Str);
        int addResult = (Integer.parseInt(liStr) + Integer.parseInt(l2Str));
        System.out.println("result " +  addResult );
        String l3Str = "";
        int length = String.valueOf(addResult).length();
        for(int i =0;i < length;i++){
            int tem = addResult % 10;
            addResult = addResult / 10;
            l3Str +=  tem ;
        }

        return l3Str;
    }

}
