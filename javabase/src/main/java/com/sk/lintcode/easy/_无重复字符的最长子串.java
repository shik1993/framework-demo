package com.sk.lintcode.easy;

import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

public class _无重复字符的最长子串 {

    /**
     * in  abcabcbb
     * out 3
     *
     * in bbbbb
     * out 1
     *
     * in pwwkew
     *
     * out 3
     * @param args
     */
    public static void main(String[] args){
            String input = "abcabcbb";
            String input2 = "bbbbb";
            String input3 = "pwwkew";
        /**
         * 暴力解法
         */
        //int result = execResult(input);

        /**
         * 最优解法 : 滑动窗口
         */
        int result2 = execResult2(input3);
        System.out.println("最终结果为 "+ result2 );
        HashMap hashMap = new HashMap();
        hashMap.put(null,null);
        Hashtable hashtable = new Hashtable();
        hashtable.put(null,null);
    }

    /**
     * 使用滑动窗口的方式来做
     * @param input
     * @return
     */
    private static int execResult2(String input) {

        if(input == null || input.length() == 0){
            return 0;
        }
        int left = 0 ;
        int right = 0 ;
        int length = input.length();
        int max =1 ;
        Set hashSet = new HashSet();
        for(; right < length; right++){
            while (hashSet.contains(input.charAt(right))){
                hashSet.remove(input.charAt(left));
                left++;
            }
                hashSet.add(input.charAt(right));
                max = Math.max(max,right - left + 1);

        }
        return max;
    }

    /**
     * 暴力解法
     * @param input
     * @return
     */
    private static int execResult(String input) {
           if(StringUtils.isBlank(input)){
               return 0;
           }
           char[] charArray = input.toCharArray();
           int length = charArray.length;
           int max = 1;

           for (int j = 0; j < length; j++) {
               int temp = 0;
               Set set = Sets.newHashSet();
                for (int i = j; i < length; i++) {
                if (!set.contains(charArray[i])) {
                    set.add(charArray[i]);
                    if(i == length-1){
                        temp = set.size();
                        if(temp > max){
                            max = temp;
                        }
                        set.clear();
                    }
                } else {
                    temp = set.size();
                    if(temp > max){
                        max = temp;
                    }
                    set.clear();
                }
            }


           }
           return max;
    }
}
