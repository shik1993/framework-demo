package com.sk.lintcode.easy;

import java.util.HashMap;

//给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
//
// 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。
//
//
//
// 示例:
//
// 给定 nums = [2, 7, 11, 15], target = 9
//
//因为 nums[0] + nums[1] = 2 + 7 = 9
//所以返回 [0, 1]
//
// Related Topics 数组 哈希表
// 👍 9640 👎 0
public class _两数之和 {

    /**
     * 查找表法
     * 空间复杂度 O(n)
     * 时间复杂度O(n)
     * @param nums
     * @param target
     * @return
     */
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];     //1.创建一个存放结果的数组res
        HashMap<Integer,Integer> map = new HashMap<>();     //2.创建一个辅助的哈希表
        for(int i = 0; i < nums.length; i ++){      //3.遍历
            int tmp = target - nums[i];     //作差
            if(map.containsKey(tmp)){       //判断
                res[0] = map.get(tmp);
                res[1] = i;
                break;
            }
            map.put(nums[i],i);
        }
        return res;     //返回
    }

//













/**
 *感悟:
 * 这道题由于数组是无序的,所以不能采用游标的方式从数组的左侧和右侧同时向中间开进
 * 但是我们发现:题目要求两个数之和达到某个target,也就是数字A = target - B即可.返回 A和 B
 *
 *
 */



//给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
//
// 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。
//
//
//
// 示例:
//
// 给定 nums = [2, 7, 11, 15], target = 9
//
//因为 nums[0] + nums[1] = 2 + 7 = 9
//所以返回 [0, 1]
//
// Related Topics 数组 哈希表




    public static void main(String[] args){
        int[] nums = new int[]{2,7,11,15} ;
        int target = 23 ;

        int[] result  = execResult(nums,target);
        System.out.println(result[0]+","+result[1]);
    }

    /**
     * 暴力破解法
     * 空间复杂度 O(n*n)
     * 时间复杂度 O(n)
     * @param nums
     * @param target
     * @return
     */
    private static int[] execResult(int[] nums, int target) {
        if(nums == null){
            throw new RuntimeException("nums array is null");
        }
        int[] result = new int[2];
        for(int i =0;i< nums.length-1;i++){
            for(int j =i+1 ; j <nums.length ; j++){
                if(nums[i] + nums[j] == target){
                    result[0] = nums[i];
                    result[1] = nums[j];
                    return result ;
                }

            }
        }
        throw new RuntimeException("not result");
    }


}



