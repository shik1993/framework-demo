package com.sk.lintcode.easy;

//给出一个 32 位的有符号整数，你需要将这个整数中每位上的数字进行反转。
//
// 示例 1:
//
// 输入: 123
//输出: 321
//
//
// 示例 2:
//
// 输入: -123
//输出: -321
//
//
// 示例 3:
//
// 输入: 120
//输出: 21
//
//
// 注意:
//
// 假设我们的环境只能存储得下 32 位的有符号整数，则其数值范围为 [−231, 231 − 1]。请根据这个假设，如果反转后整数溢出那么就返回 0。
// Related Topics 数学
// 👍 2363 👎 0

public class _整数反转 {
    public int reverse(int x) {
        int result = 0;
        while(x != 0){
            int tmp = x % 10;
            if(result > 214748364 || (result == 214748364 && tmp >7)){
                return result;
            }
            if(result < -214748364 || (result == -214748364 && tmp <-8)){
                return result;
            }
            result = result * 10 + tmp ;
            x = x / 10 ;
        }
        return result;
    }

    public static void main(String[] args){
        //int[] testArray = new int[]{2,7,14,15};
        int value = -4321000;
        _整数反转 solution = new _整数反转();
        int result = solution.reverse(value);
        System.out.println(": "+result);
    }

    /**
     * 解题思路：
     *
     * 首先我们想一下，怎么去反转一个整数？
     * 用栈？
     * 或者把整数变成字符串，再去反转这个字符串？
     * 这两种方式是可以，但并不好。实际上我们只要能拿到这个整数的 末尾数字 就可以了。
     * 以12345为例，先拿到5，再拿到4，之后是3，2，1，我们按这样的顺序就可以反向拼接处一个数字了，也就能达到 反转 的效果。
     * 怎么拿末尾数字呢？好办，用取模运算就可以了
     *
     *
     */
}