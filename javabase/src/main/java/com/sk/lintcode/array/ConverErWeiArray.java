package com.sk.lintcode.array;

import java.util.Arrays;

/**
 * 1.二维数组的对称翻转
 * 2.二维数组的90度转换,180度
 *
 */
public class ConverErWeiArray {

    public static void main(String[] args){
        // 3*3
//        int [][] arr1 = new int[3][];
//        arr1[0] = new int[]{1,2,3};
//        arr1[1] = new int[]{4,5,6};
//        arr1[2] = new int[]{7,8,9};

        // 4*4
        int [][] arr1 = new int[4][];
        arr1[0] = new int[]{1,2,3,4};
        arr1[1] = new int[]{5,6,7,8};
        arr1[2] = new int[]{9,10,11,12};
        arr1[3] = new int[]{13,14,15,16};

        //初始化打印
        System.out.println("初始化打印: ");
        print(arr1);
        //int[][] arrClone = arr1.clone();//浅拷贝 错的
        int[][] arrClone = new int[4][];
        for(int k=0;k<arr1.length;k++){
            arrClone[k] = Arrays.copyOf(arr1[k],arr1[k].length);
        }
        //对称打印
        System.out.println("正对称打印: ");
        convert(arr1);
        print(arr1);
        System.out.println("中线对折打印: ");
        print(arrClone);
        System.out.println("---");
        int[][] arr2 =convert2(arrClone);
        print(arr2);
        //正向旋转90度
        System.out.println("正向旋转180度: ");
        int[][] arr3 =convert3(arrClone);
        print(arr3);
        System.out.println("正时针旋转90度打印: ");
        print(arrClone);
        System.out.println("---");
        int[][] arr4 =convert4(arrClone);
        print(arr4);
    }

    private static int[][] convert4(int[][] arr) {
        int[][] arr3 = new int[4][4];
        for(int i = 0;i<arr.length;i++){
            for(int j = 0; j< arr[i].length;j++){
                arr3[i][j] = arr[3-j][i];
            }
        }
        return arr3;
    }

    private static int[][] convert3(int[][] arr) {
        int[][] arr3 = new int[4][4];
        for(int i = 0;i<arr.length;i++){
            for(int j = 0; j< arr[i].length;j++){
                arr3[i][j] = arr[3-j][3-i];
            }
        }
        return arr3;
    }

    public static void convert(int[][] arr) {
        for(int i = 0;i<arr.length;i++){
            for(int j = 0; j< i;j++){
                    int t = arr[i][j];
                    arr[i][j] = arr[j][i];
                    arr[j][i] = t;
            }
        }
    }

    public static int[][] convert2(int[][] arr) {
        int[][] arr2 = new int[4][4];
        for(int i = 0;i<arr.length;i++){
            for(int j = 0; j< arr[i].length;j++){
                arr2[i][j] = arr[3-i][j];
            }
        }
        return arr2;
    }

    public static void print(int[][] arr){
        for(int i = 0;i<arr.length;i++){
            for(int j = 0; j< arr[i].length;j++){
                System.out.print("("+i+","+j +")="+ arr[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

}
