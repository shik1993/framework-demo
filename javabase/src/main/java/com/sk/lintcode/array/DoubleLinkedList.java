package com.sk.lintcode.array;

public class DoubleLinkedList {


    static class Node{
        public int value;
        public Node pre;
        public Node next;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public Node getPre() {
            return pre;
        }

        public void setPre(Node pre) {
            this.pre = pre;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public Node(int value){
            this.value = value;
        }
    }


    public Node head ;
    public Node tail ;

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getTail() {
        return tail;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    /**
     *  头插
     * @param value
     */
    public void addFirst(int value){
        Node node = new Node(value);
        if(this.getHead() == null){//空list
            this.setTail(node);
            this.setHead(node);
        } else {//有值
            node.setNext(this.getHead());
            this.getHead().setPre(node);
            this.setHead(node);
        }
    }

    public void addTail(int value){
        Node node = new Node(value);
        if(this.getTail() == null){//空list
            this.setTail(node);
            this.setHead(node);
        } else {//有值
            this.getTail().setNext(node);
            node.setPre(this.getTail());
            this.setTail(node);
        }
    }

    public boolean contains(int key){
        Node cur = this.getHead();
        while (cur != null){
            if(cur.getValue() == key){
                return true;
            }
            cur = cur.getNext();
        }
        return false;
    }

    public int size(){
        int size = 0 ;
        Node cur = this.getHead();
        while (cur != null){
            size++;
            cur = cur.getNext();
        }

        return size;
    }

    public void add(int index , int data){
        if(index < 0 || index >this.size()){
            throw new RuntimeException("index crash");
        }
        Node cur = this.getHead();
        int step =0;
        Node node = new Node(data);
        if(cur == null){
            this.setHead(node);
            this.setTail(node);
        }else {
            while (cur != null){

                if(index == 0){//head
                    node.setNext(this.getHead());
                    this.getHead().setPre(node);
                    this.setHead(node);
                    return;
                }
                if(cur.getNext() == null){//tail
                    this.getTail().setNext(node);
                    node.setPre(this.getTail());
                    this.setTail(node);
                    return;
                }
                if(step == index){
                    // add
                    Node pre = cur.getPre();
                    Node next = cur.getNext();

                    pre.setNext(node);
                    node.setPre(pre);

                    node.setNext(cur);
                    cur.setPre(node);
                    return;
                }
                cur = cur.getNext();
                step++;
            }
        }

    }

    public void remove(int key){
        Node cur = this.getHead();
        while (cur != null){
            if(cur.getValue() == key){
                //delete
                if(cur.getPre() == null){//delete head
                    cur.getNext().setPre(null);
                    this.setHead(cur.getNext());

                }else if(cur.getNext() == null){//delete tail
                    cur.getPre().setNext(null);
                    this.setTail(cur.getPre());
                }else {//delete node
                    // delete
                    Node pre = cur.getPre();
                    Node next = cur.getNext();

                    pre.setNext(next);
                    next.setPre(pre);

                    cur.setNext(null);
                    cur.setPre(null);
                    return;
                }
            }
            cur = cur.getNext();
        }
    }

    public void display(){
        Node cur = this.getHead();
        while (cur != null){
            System.out.println("cur value : " + cur.getValue());
            cur = cur.getNext();
        }
    }

    public void removeAllkey(int key){
        Node cur = this.getHead();
        while (cur != null){
            if(cur.getValue() == key){
                cur = cur.getPre();
                remove(key);
            }
            cur = cur.getNext();
        }
    }

    public void clear(){
        while (this.getHead() != null){
            Node cur = this.getHead().getNext();
            this.getHead().setPre(null);
            this.getHead().setNext(null);
            this.setHead(cur);
        }
        this.setTail(null);
    }



    public static void main(String[] args){
        DoubleLinkedList doubleLinkedList = new DoubleLinkedList();
        System.out.println("add First :");
        doubleLinkedList.addFirst(1);
        doubleLinkedList.addFirst(2);
        doubleLinkedList.addFirst(3);
        doubleLinkedList.addFirst(4);
        doubleLinkedList.addFirst(5);
        doubleLinkedList.addFirst(6);
        doubleLinkedList.addFirst(7);
        doubleLinkedList.display();
        doubleLinkedList.clear();
        System.out.println("clear add First :");
        doubleLinkedList.addTail(1);
        doubleLinkedList.addTail(2);
        doubleLinkedList.addTail(3);
        doubleLinkedList.addTail(4);
        doubleLinkedList.addTail(5);
        doubleLinkedList.addTail(6);
        doubleLinkedList.addTail(7);
        doubleLinkedList.display();
        System.out.println("contains 6:"+doubleLinkedList.contains(6));
        System.out.println("contains 8:"+doubleLinkedList.contains(8));
        System.out.println("size :"+doubleLinkedList.size());
        doubleLinkedList.add(0,0);
        System.out.println("add 0,0 :");
        doubleLinkedList.display();
        System.out.println("add 2,10 :");
        doubleLinkedList.add(2,10);
        doubleLinkedList.display();
        System.out.println("add 8,11 :");
        doubleLinkedList.add(8,11);
        doubleLinkedList.display();
        System.out.println("delete 0 :");
        doubleLinkedList.remove(0);
        doubleLinkedList.display();
        System.out.println("delete 11 :");
        doubleLinkedList.remove(11);
        doubleLinkedList.display();
        System.out.println("delete 10 :");
        doubleLinkedList.remove(10);
        doubleLinkedList.display();
        System.out.println("delete 100 :");
        doubleLinkedList.remove(100);
        doubleLinkedList.display();
        System.out.println("add 4,5 :");
        doubleLinkedList.add(4,5);
        doubleLinkedList.display();
        System.out.println("delete 5 :");
        doubleLinkedList.removeAllkey(5);
        doubleLinkedList.display();
    }

}