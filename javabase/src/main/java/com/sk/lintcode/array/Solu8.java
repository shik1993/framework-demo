package com.sk.lintcode.array;

import java.util.Arrays;

public class Solu8 {
    /**
     * 寻找一个数组中成绩最大的三个数,这个数组有正有负,要求算法复杂度最低
     * 举例:   -1,-2,-3,0,1,1,2,3,4
     * 要求:算法的时间复杂度为 O(n)
     */
    public static void main(String[] args){
        //解决方案一
        Solu8 solu8 = new Solu8();
        int[] input = new int[]{-1,-2,-3,-4,1,1,2,3,4};
        int[] result = solu8.getMaxThree(input);
        for(int num :result){
            System.out.println(num);
        }
        System.out.println("***********************");
        //解决方案二
        input = new int[]{-1,-2,-3,-4,1,1,2,3,4};
        int[] result2 = solu8.getMaxThree(input);
        for(int num :result2){
            System.out.println(num);
        }
    }

    public int[] getMaxThree(int[] input) {
        //第一个思路:对于一个有序数组,就是寻找最后边的三个数,或者最右边的一个数+最左边的两个数
        int[] result = new int[3];
        if(input == null || input.length <=2)
            return result;
        Arrays.sort(input);

         if((input[input.length -1 ] * input[input.length -2 ] * input[input.length -3])
                > (input[input.length -1 ] * input[0] * input[1])){
             result[0] = input[input.length -1 ];
             result[1] = input[input.length -2 ];
             result[2] = input[input.length -3 ];
         }else{
             result[0] = input[input.length -1 ];
             result[1] = input[ 0 ];
             result[2] = input[ 1 ];
         }
         return result;


    }

    public int[] getMaxThree2(int[] input){
        //第二个思路:既然要时间复杂度最低,肯定是不能先排序,那么就需要我们直接找 五个数
        // 最大的三个正数 或者 最大的 两负一正
        int[] result = new int[3];
        if(input == null || input.length <=2)
            return result;
        int max1 = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;
        int max3 = Integer.MIN_VALUE;

        int min1 = Integer.MAX_VALUE;
        int min2 = Integer.MAX_VALUE;

        for(int i=0;i<input.length;i++) {
            if (input[i] > max1){
                max3 = max2;
                max2 = max1;
                max1 = input[i];
            }else if(input[i] > max2){
                max3 = max2;
                max2 = input[i];
            }else if(input[i] > max3){
                max3 = input[i];
            }

            if(input[i] < min1){
                min2 = min1;
                min1 = input[i];
            }else if(input[i] < min2){
                min2 = input[i];
            }
        }
        if(max1*max2*max3 > max1*min1*min2){
            result[0] = max1;
            result[1] = max2;
            result[2] = max3;
        }else {
            result[0] = max1;
            result[1] = max2;
            result[2] = max3;
        }

        return result;
    }


}
