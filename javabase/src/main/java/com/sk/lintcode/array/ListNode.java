package com.sk.lintcode.array;

/**
 * 模拟一个简单的单向链表
 */
public class ListNode {

    private ListNode next ;

    private Integer values ;

    public ListNode(){
    }

    public ListNode(Integer values){
        this.values = values ;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public Integer getValues() {
        return values;
    }

    public void setValues(Integer values) {
        this.values = values;
    }


    public static void main(String[] args){
        ListNode listNode0 = new ListNode(0);
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(4);

        listNode4.setNext(null);
        listNode3.setNext(listNode4);
        listNode2.setNext(listNode3);
        listNode1.setNext(listNode2);
        listNode0.setNext(listNode1);

        printListNode(listNode0);
    }

    public static void printListNode(ListNode listNode) {
        while (listNode != null) {
            System.out.println(listNode.getValues());
            listNode = listNode.getNext();
        }
    }
}
