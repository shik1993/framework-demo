package com.sk.lintcode.str;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * 实现一下string的  split
 * 注意边界和性能
 *input: str="acbbbc", c='b'
 * output:["ac","c"]
 *
 * charAt
 *
 */
public class Split {

    public static void main(String[] args){
        Split split = new Split();
        String str = "acbbbc";

        List<String> result = split.getResult(str,'b');
        System.out.println(result);

        String str1 = "abcbebb";
        List<String> result1 = split.getResult(str1,'b');
        System.out.println(result1);
        System.out.println("for test subStr 5,5 : "+str.substring(5,5));
    }

    static List<String> getResult(String str,char split) {
        int length = str.length();
        List result = Lists.newArrayList();
        if (null == str || str.length() < 1) {
            return result;
        }
        //StringBuffer sb = new StringBuffer();
        int index = 0;
        for (int i = 0; i < length; ) {
            char temp = str.charAt(i);
            if(temp != split){
                i++;
                if(i >= length){
                    System.out.println("要加入的元素 "+str.substring(index,i));
                    result.add(str.substring(index, i));
                }
            }else{
                if(index < i) {
                    System.out.println("要加入的元素 "+str.substring(index,i));
                    result.add(str.substring(index, i));
                }
                index = ++i;

            }



        }
        return result;
    }
}
