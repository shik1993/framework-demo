package com.sk.lintcode;

/**
 * 一个有序的整形数组，给定一个数，在数组中找出两个数的和等于这个数，并打印出来
 * Created by shik on 2019/7/27.
 * 首选,数组已经是有序得了,所以不需要先排序
 * 其次,按照排序算法里面的交换次数,选择排序次数为N,优于冒泡排序里的N*N
 *
 */
public class seachData {

    public static void main(String[] args){
        int[] arr = new int[]{-9,-7,-3,-1,0,1,3,5,7,9};
        seachData seachData = new seachData();
        seachData.advice(arr,0,0,arr.length-1);
    }

    public void advice(int[] source,int target,int min, int max){
        int index = 0;
        int[] result = new int[2];
        while(min <= max) {
            if (source[min] + source[max] == target) {
                result[0] = source[min];
                result[1] = source[max];
                System.out.println(result[0]+ " | " +result[1]);
                min ++ ;
            } else if (source[min] + source[max] < target) {
                min++;
            } else {
                max--;
            }
        }
    }
}
