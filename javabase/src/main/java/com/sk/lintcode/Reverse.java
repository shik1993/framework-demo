package com.sk.lintcode;

import java.util.Stack;

/**
 * 反转类
 *解题思路：
 * 入栈，出栈 实现翻转string
 */
public class Reverse {

    public static void main(String[] args){
        Reverse appTest = new Reverse();
        int result1 = appTest.reverseInteger(900);
        System.out.println(result1);
        char result2 = appTest.lowercaseToUppercase('c');
        System.out.println(result2);
        int[] sourceArray = new int[]{7,6,8,5,4,7,56,45};
        appTest.display(sourceArray);
        appTest.sortIntegers(sourceArray);
    }
    /**
     * 问题描述：
     * 37. 反转一个只有3位数的整数。
     样例 1:
     输入: number = 123
     输出: 321
     样例 2:
     输入: number = 900
     输出: 9
     注意事项
     你可以假设输入一定是一个只有三位数的整数，这个整数大于等于100，小于1000。
     ***************************************************************
     * Stack的基本使用
     初始化
     Stack stack=new Stack
     判断是否为空
     stack.empty()
     取栈顶值（不出栈）
     stack.peek()
     进栈
     stack.push(Object);
     出栈
     ***************************************************************
     * @param number: A 3-digit number.
     * @return: Reversed number.
     */
    public int reverseInteger(int number) {
        String temp = String.valueOf(number);
        Stack stack = new Stack();
        stack.push(temp.charAt(0));
        stack.push(temp.charAt(1));
        stack.push(temp.charAt(2));

        char[] tempChar =new char[3];
        tempChar[0] = (char)stack.pop();
        tempChar[1] = (char)stack.pop();
        tempChar[2] = (char)stack.pop();

        int result = Integer.valueOf(String.valueOf(tempChar));
        return result;
    }


    /**
     * 将一个字符由小写字母转换为大写字母

     样例
     样例 1:

     输入: 'a'
     输出: 'A'
     样例 2:

     输入: 'b'
     输出: 'B'
     * @param character: a character
     * @return: a character
     */
    public char lowercaseToUppercase(char character) {
        // write your code here
        String charStr = String.valueOf(character);
        String charUpStr = charStr.toUpperCase();
        return charUpStr.charAt(0);
    }


    /**
     * 数组排序： 任何一个复杂度O（n*n）的算法： 冒泡、选择排序
     * @param A: an integer array
     * @return: nothing
     */
    public void sortIntegers(int[] A) {
        // write your code here
        choice(A);
        //maopao(A);
    }

    private void choice(int[] A) {
        //选择排序： O(n*n),交换次数最大：n
        for(int i = 0 ; i < A.length-1; i++){
            int temp = A[i];
            for(int j = i+1;j < A.length ; j++){
                if(temp > A[j]){
                    temp = A[j];
                }else{
                    temp = temp;
                }
                if(j == A.length-1){
                    A[i] = temp;
                }
            }
        }
        display(A);
    }

    private void maopao(int[] A) {
        //冒泡排序： O(n*n),交换次数最大：n+ (n-1) + (n-2) + .... 1
        for(int i = 0 ; i < A.length-1; i++){
            int temp = 0;
            for(int j = i+1;j < A.length ; j++){
                if(A[i] > A[j]){
                    temp = A[i];
                    A[i] = A[j];
                    A[j] = temp;
                }
            }
        }
        display(A);
    }

    public void display(int[] target){
        for(int t : target){
            System.out.print(t+" ");
        }
        System.out.println();
    }
}


