package com.sk.lintcode;

public class Solu2 {


    /**

     给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。

     你可以假设每种输入只会对应一个答案。但是，你不能重复利用这个数组中同样的元素。

     示例:

     给定 nums = [2, 7, 11, 15], target = 9

     因为 nums[0] + nums[1] = 2 + 7 = 9
     所以返回 [0, 1]
     */


    public int[] getPoint(int[] nums,int target){
        int[] result =new int[2];
        int size = nums.length;
        int min = 0;
        int max = size - 1;
        while (min < max) {
            if (nums[min] + nums[max] == target) {
                result[0] = min;
                result[1] = max;
                return result;
            } else if (nums[min] + nums[max] < target){
                min++;
            }else {
                max --;
            }
        }
        return null;
    }

    public static void main(String[] args){
        Solu2 solu2 = new Solu2();
        //int[] intArr = new int[5];
       int[] intArr = {1,4,6,7,8,9,19};
        int[] result = solu2.getPoint(intArr,8);
        for(int i = 0 ; i < result.length;i++){
            System.out.println(result[i]);
        }
    }
}
