package com.sk.boot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.types.RedisClientInfo;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 测试加载类的testCase
 * 一个基于dev环境的可用的 redis环境
 *
 */
@RunWith(SpringRunner.class)
@Profile("dev")
@SpringBootTest
public class RedisApplicationTests {
	Logger logger = LoggerFactory.getLogger(getClass());

	public static final String Key ="shik1";

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	@Test
	public void testRedisService(){
			System.out.println("testRedisService: start");


		Boolean result = stringRedisTemplate.hasKey(Key);
			if(result){
				System.out.println("redis get "+Key + " :"+stringRedisTemplate.opsForValue().get(Key));
				return;
			}
		// 插入缓存
		String value = "oooo";
		stringRedisTemplate.opsForValue().set(Key, value, 100, TimeUnit.SECONDS);
		logger.info("testRedisService.insert() : 插入缓存 >> " + value);

	}

}
