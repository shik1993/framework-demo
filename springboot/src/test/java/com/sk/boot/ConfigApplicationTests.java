package com.sk.boot;

import com.sk.boot.config.po.BookComponent;
import com.sk.boot.config.po.BookProperties;
import com.sk.boot.config.po.Person;
import com.sk.boot.service.HelloService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试加载类的testCase
 */
@RunWith(SpringRunner.class)
@Profile("dev")
@SpringBootTest
public class ConfigApplicationTests {

	@Autowired
	BookProperties bookProperties;

	@Autowired
	BookComponent bookComponent;

	@Autowired
	Person person;

	@Autowired
	ApplicationContext ioc;

	@Autowired
	HelloService helloService;

	@Test
	public void testHelloService(){
		boolean b = ioc.containsBean("helloService");
		System.out.println("testHelloService: "+b);

		System.out.println("testHelloService: "+helloService.getValue());
	}

	@Test
	public void contextLoads() {
		/**
		 * 自测情况发现: 如果Person.java添加
		 * @PropertySource(value = {"classpath:person.properties"})
		 * 会从执行的文件中拿数据,如果application.properties中有数据,会覆盖 classpath:person.properties
		 *
		 */
		System.out.println(person);
	}

	@Test
	public void testBookProperties() {
		Assert.assertEquals(bookProperties.getName(),"[Spring Boot 2.x Core Action]");
		Assert.assertEquals(bookProperties.getWriter(),"BYSocket");
	}

	@Test
	public void testBookComponent() {
		Assert.assertEquals(bookComponent.getName(),"[Spring Boot 2.x Core Action]");
		Assert.assertEquals(bookComponent.getWriter(),"BYSocket");
	}
}
