package com.sk.boot.service;

public class HelloService {

    private  volatile Integer value = 1;

    {
        value = 10;
    }

    public  Integer getValue() {
        return value;
    }

    public  void setValue(Integer value) {
        value = value;
    }
}

