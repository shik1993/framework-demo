package com.sk.boot;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 */
@EnableSwagger2Doc // 开启 Swagger
@SpringBootApplication
public class MainApplication {

    public static void main(String[] args) {
        //Spring应用启动
        SpringApplication.run(MainApplication.class,args);
    }
}
