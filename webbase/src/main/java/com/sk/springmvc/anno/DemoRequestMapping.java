package com.sk.springmvc.anno;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *  运行期有效
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface DemoRequestMapping {
    public String value();
}
