package com.sk.springmvc.handler;

import java.lang.reflect.Method;

public class Handler {
    private Method mh;//方法对象
    private Object obj;//方法所在类对象

    public Handler(Method mh, Object obj) {
        this.mh = mh;
        this.obj = obj;
    }
    public Method getMh() {
        return mh;
    }
    public void setMh(Method mh) {
        this.mh = mh;
    }
    public Object getObj() {
        return obj;
    }
    public void setObj(Object obj) {
        this.obj = obj;
    }

}
